<?php
	## call const start
	require_once dirname(__FILE__) . '/libs/const.php';

	## import custom route start
	require_once dirname(__FILE__) . '/libs/route/handler.php' ;

	## adding navigation support of admin menu start
	add_action( 'after_setup_theme', '_tana_register_my_menu' );
	
	function _tana_register_my_menu() {
		register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) );
	}
	## adding navigation support of admin menu end

	## admin page setting start
	add_action( 'admin_init', '_tana_admin_init');
	
	function _tana_admin_init() {
		register_taxonomy_for_object_type( 'category', 'page' );
	}
	## admin page setting end

	## add 
	function _tana_load_js() {
		/**
		 * Prints scripts or data before the closing body tag on the front end.
		 *
		 * @since 1.5.1
		 */
		echo '
		<script src="'.get_template_directory_uri().'/js/slick.min.js"></script>
		<script src="'.get_template_directory_uri().'/js/slick-animation.min.js"></script>
		<script src="'.get_template_directory_uri().'/js/plugins.js"></script>
		<script src="'.get_template_directory_uri().'/js/selectivizr-min.js"></script>
		<script src="'.get_template_directory_uri().'/js/common.js"></script>
		';
		do_action( '_tana_load_js' );
	}
	## end start

	# remove p tags in editor
	remove_filter( 'the_content', 'wpautop' );
	## end

	## hide admin bar if not admin start
	add_action('after_setup_theme', '_tana_remove_admin_bar');
	function _tana_remove_admin_bar() {
		if (!current_user_can('administrator') && !is_admin()) {
			show_admin_bar(false);
		}
	}
	## end

	## add custom admin menu start
	add_action('admin_menu', '_tana_register_admin_menu');
	function _tana_event_news_fnc() {
		require_once(dirname(__FILE__) . '/libs/admin/event_news.php');
	}
	function _tana_register_admin_menu() {
		add_menu_page(
			'Event&News', //page title
			'Event&News', //menu title
			'manage_options', // capability
			'event_news', // menu slug
			'_tana_event_news_fnc'
		);
	}
	## end

	## start menu
	function _tana_print_menu() {
		$menuLocations = get_nav_menu_locations();
		$menuID = $menuLocations['primary'];
		$primaryNav = wp_get_nav_menu_items($menuID);

		foreach( $primaryNav as $nav ) {
			$_page = explode("/", $nav->url);
			if (empty($_page[5])) {
				$_page[5] = 'home';
			}
			echo "<li data-page-name='$_page[5]'><a href='$nav->url'>$nav->title</a></li>";
		}do_action('_tana_print_menu');
	}
	## end menu

	## create type event news
	function _tana_get_head_metas() {
		if ( array_key_exists('pid', $_GET) && is_numeric($_GET['pid']) && intval($_GET['pid']) > 0 ) {
			global $wpdb;
			$_title = $wpdb -> get_var( $wpdb -> prepare("SELECT post_title FROM wp_posts WHERE ID = %d", $_GET['pid']) );

			echo sprintf('<meta name="description" content="%s">', $_title);
			echo "\n";
			echo sprintf('<meta name="Keywords" content="%s">', $_title);
			echo "\n";
			echo sprintf('<meta property="og:description" content="%s">', $_title);
			echo "\n";
		}
		do_action('_tana_get_head_metas');
	}

	## add shortcode
	function path_shortcode() {// uri 경로
		return get_template_directory_uri();
	}
	add_shortcode('path', 'path_shortcode');

	function home_shortcode() {// home 경로
		return 'http://localhost/wp_project/mjallskin/';
	}
	add_shortcode('path_home', 'home_shortcode');

	if (!is_admin()) {// include
		//include dirname(__FILE__) . '/admin.php';
	//} else {
		function include_shortcode_function($attrs, $content = null) {
			if (isset($attrs['file'])) {
				$file = strip_tags($attrs['file']);
				if ($file[0] != '/')
					$file = ABSPATH . $file;
		
				ob_start();
				include($file);
				$buffer = ob_get_clean();
				$options = get_option('includeme', array());
				if (isset($options['shortcode'])) {
					$buffer = do_shortcode($buffer);
				}
			} else {
				$tmp = '';
				foreach ($attrs as $key => $value) {
					if ($key == 'src') {
						$value = strip_tags($value);
					}
					$value = str_replace('&amp;', '&', $value);
					if ($key == 'src') {
						$value = strip_tags($value);
					}
					$tmp .= ' ' . $key . '="' . $value . '"';
				}
				$buffer = '<iframe' . $tmp . '></iframe>';
			}
			return $buffer;
		}
		
		add_shortcode('include', 'include_shortcode_function');
	}
	
?>
