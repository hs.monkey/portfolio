<?php
/**
 * Template Name: test
 */
    get_header();
    _tana_load_js();
    if ( have_posts() ) {
        while ( have_posts() ) {
            the_post();
            the_content();
        }
    }
    include '/content/guide.php';
    get_footer();
?>
