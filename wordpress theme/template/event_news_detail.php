<!-- s: .container -->
<div class="container cont_body" id="container">
    <div class="inner">
        <div class="page-title">
            <h2>Event &amp; News</h2>
        </div>
        <div class="content_wrap">
            <div class="content_bg"></div>
            <div class="content">
                <div class="tab_menu">
                    <h3 class="item_t"></h3>
                </div>

                <aritcle class="news_article">
                    <div class="news_article_header">
                        <h4 class="title"></h4>
                        <span class="date"></span>
                    </div>
                    <div class="news_article_body"></div>
                </aritcle>

                <div class="btn_area">
                    <div class="fl">
                        <a class="btn_line btn_prev_page" href="#" onclick="_en.goList();" >List</a>
                    </div>
                    <div class="fr">                                
                        <a class="btn_line ico_only sns" href="https://telegram.me/share/url?url=" target="_blank">
                            <i class="fa fa-telegram"></i>
                            <span class="hide">Telegram</span>
                        </a>
                        <a class="btn_line ico_only sns" href="https://twitter.com/home?status=" target="_blank">
                            <i class="fa fa-twitter"></i>
                            <span class="hide">Twitter</span>
                        </a>
                        <a class="btn_line ico_only sns" href="https://www.facebook.com/sharer/sharer.php?u=" target="_blank">
                            <i class="fa fa-facebook-f"></i>
                            <span class="hide">Facebook</span>
                        </a>                                
                        <a class="btn_line ico_only" href="#" onclick="_en.print(event);">
                            <i class="fa fa-print"></i>
                            <span class="hide">Print</span>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- s: #footer -->
    <footer id="footer"></footer>
    <!-- e: #footer -->
</div>
<!-- e: .container -->

<script src="http://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
<script src="/js/_custom_lib.js" ></script>
<script>
    var _en = {
        page: 1
        ,container: document.getElementById('container')
        ,pid: 0
        ,init: function() {
            var self = this;
            var _p = _common.getParams();
            if (_p.hasOwnProperty('_p')) {
                self.page = parseInt(_p['_p']);
            }
            if ( !_p.hasOwnProperty('pid') || typeof _p['pid'] === 'undefined' || !_common.isNum(_p['pid']) ) {
                alert('잘못된접근입니다.');
                self.goList();
            }
            self.pid = parseInt(_p['pid']);
            self.searchOpt = {pid: self.pid};
            self.load();
        }
        ,goList: function() {
            var self = this;
            window.location.href = '/event_news?_p={0}'.format(self.page);
        }
        ,print: function(ev) {
            var self = this;
            var body = document.body;
            var origin = document.body.innerHTML;
            _common.cancelPrevent(ev);
            html2canvas(document.querySelector('div.content_wrap')).then(canvas => {
                _common.removeChilds(body);
                body.appendChild(canvas);
                window.print();
                _common.removeChilds(body);
                body.innerHTML = origin;
            });
        }
        ,load: function(){
            var self = this;

            var meta, opt;
            var _data = {'code': 'get'};
            if (typeof self.searchOpt === 'undifined' || self.searchOpt === null) {
                _data['data'] = {}
            } else {
                _data['data'] = self.searchOpt;
            }

            jQuery.ajax({
                url: '/v1/api'
                ,method: 'GET'
                ,data: 'data=' + JSON.stringify(_data)
                ,success: function(res) {
                    res = JSON.parse(res);
                    if (res.count < 1) {
                        alert('잘못된접근입니다.');
                        self.goList();
                        return;
                    }

                    self.container.querySelector('h3.item_t').innerText = res.term_name;
                    self.container.querySelector('h4.title').innerText = res.post_title;
                    self.container.querySelector('span.date').innerText = res.post_date;
                    self.container.querySelector('div.news_article_body').innerHTML = _common.nl2br(res.post_content);
                    // set sns link
                    [].forEach.call(self.container.querySelectorAll('div.fr a.sns'), function(a) {
                        a.setAttribute(
                            'href'
                            ,'{0}{1}/event_news?pid={2}'.format(a.getAttribute('href'), window.location.origin, res.pid)
                        )
                    });

                }
            });
        }
    }

    _en.init();
</script>
