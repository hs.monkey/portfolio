<!-- s: .container -->
<div class="container cont_body">
    <div class="inner">
        <div class="page-title">
            <h2>Event &amp; News</h2>
        </div>
        <div class="content_wrap">
            <div class="content_bg"></div>
            <div class="content">
                <nav class="tab_menu">
                    <ul class="tab_menu_list layout3">
                        <li class="item">
                            <a href="" class="item_t" onclick="_en.selCat(this,event,'program_event')" >Program &amp; Event</a>
                        </li>
                        <li class="item">
                            <a href="" class="item_t" onclick="_en.selCat(this,event,'notice')" >Notice</a>
                        </li>
                        <li class="item">
                            <a href="" class="item_t" onclick="_en.selCat(this,event,'press_release')" >Press Release</a>
                        </li>
                    </ul>
                </nav>
                <!-- s: #event-news-lists -->
                <div class="info_list" id="event-news-lists"></div>
                <!-- e: #event-news-lists -->

                <div class="btn_area">
                    <div class="fl">
                        <a class="btn_line btn_prev_page" href="#" onclick="_en.prevPage(this,event);">
                            <i class="fa fa-angle-left"></i>Prev page</a>
                    </div>
                    <div class="fr">
                        <a class="btn_line btn_next_page" href="#" onclick="_en.nextPage(this,event);">Next page
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- e: .container -->
<script src="/js/_custom_lib.js" ></script>
<script>
    var _en = {
        page: 1
        ,total: 0
        ,limit: 10
        ,contentLength: 80
        ,searchOpt: null
        ,listBody: document.getElementById('event-news-lists')
        ,init: function() {
            var _p = _common.getParams();
            if (_p.hasOwnProperty('_p')) {
                this.page = parseInt(_p['_p']);
            }
            this.load();
        }
        ,load: function(){
            var self = this;

            var _data = {'code': 'lists'};
            if (typeof self.searchOpt === 'undifined' || self.searchOpt === null) {
                _data['data'] = {}
            } else {
                _data['data'] = self.searchOpt;
            }
            _data['data']['page'] = self.page;
            _data['data']['limit'] = self.limit;

            jQuery.ajax({
                url: '/v1/api'
                ,method: 'GET'
                ,data: 'data=' + JSON.stringify(_data)
                ,success: function(res) {
                    res = JSON.parse(res);
                    if (res.count < 1) {
                        return;
                    }

                    self.page = res.page;
                    self.total = (res.count - 1) / self.limit + 1;
                    self.printList(self, res);
                }
            });
        }
        ,nextPage: function(el, ev) {
            _common.cancelPrevent(ev);
            var self = this;
            self.page = self.page + 1;
            if (self.page > self.total) {
                self.page = self.page - 1;
                alert('마지막입니다.');
                return;
            }
            self.load();
        }
        ,prevPage: function(el, ev) {
            _common.cancelPrevent(ev);
            var self = this;
            if (self.page - 1 < 1) {
                alert('처음입니다.');
                return;
            }
            self.page = self.page - 1;
            self.load();
        }
        ,printList: function(self, res) {
            var dl, dt, a, dd, opt, content;

            _common.removeChilds(self.listBody);
            [].forEach.call( res.data, function(row, idx) {
                opt = {'class': 'lst'};
                dl = _common.newEl('dl', opt);

                opt = {};
                dt = _common.newEl('dt', opt);

                opt = {'class': 'tit', href: '/event_news?pid={0}&_p={1}'.format(row.pid, self.page)};
                a = _common.newEl('a', opt);
                a.innerText = row.post_title;
                dt.appendChild(a);
                dl.appendChild(dt);

                opt = {};
                dd = _common.newEl('dd', opt);
                content = _common.removeMarkup(row.post_content);
                content = _common.cutByLen(content, self.contentLength);
                if (_common.getByLen(content) > self.contentLength) {
                    content = content + '...';
                }
                dd.innerText = content;
                dl.appendChild(dd);

                self.listBody.appendChild(dl);
            });
        }
        ,selCat: function(el, ev, category) {
            _common.cancelPrevent(ev);
            [].forEach.call(el.parentNode.parentNode.querySelectorAll('li'), function(li) {
                li.className = li.className.replace(/ on/g, '');
            });
            el.parentNode.className = el.parentNode.className + ' on';
            var self = this;
            self.page = 1;
            self.searchOpt = {'category': category};
            self.load();
        }
    };

    _en.init();
</script>

