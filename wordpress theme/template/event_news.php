<?php
/**
 * Template Name: event & news
 */

$_page = 'list.php';

if ( array_key_exists('pid', $_GET) && is_numeric($_GET['pid']) && intval($_GET['pid']) > 0 ) {
    $_page = 'detail.php';
}

get_header();
_tana_load_js();

require_once(dirname(__FILE__) . '/event_news_' . $_page);

get_footer();
?>
