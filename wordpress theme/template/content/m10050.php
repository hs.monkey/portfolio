	<!-- page content 영역 -->
	<div id="content" class="content-wrap whitening-miracle">

		<section class="content-head">
			<h3 class="content-title">미라클 레이저</h3>
			<p class="title-suffix w_line">MJ올피부과는 당신에게 건강한 아름다움을 선사합니다.</p>
		</section>
		<section class="headcopy">
			<div class="tit fc-main">화이트닝과 피부결개선</div>
			<p class="subtxt">
			전체 피부톤을 한톤 업그레이드 시켜주고 피붓결을 매끄럽게 재생시켜주며 난치성 기미나 난치성 색소의 치료에 다른 치료법과 병행하여 치료했을 때 기적처럼 미백효과를 크게 발휘하는 특수 레이저 치료법입니다. 1927nm 파장대의 프랙션날 레이저 시스템으로 레이저토닝이나 미라클 레이저, 탄산가스 레이저 등의 미백시술과 함께 치료 했을 때 더 좋은 시너지를 일으킵니다.
			</p>
		</section>

		<section class="content">
			<h3 class="blind">content</h3>

			<div class="info-box">
				<ul class="box-wrap">
					<li class="box-item">
						<div class="box-title">흉터 모공축소 피부재생의 효과</div>
						<p class="box-cont">새로운 화이트닝 레이저 장비로 1927nm 파장의 레이저를 피부에 조사하여 기존의 치료가 힘들었던 기미 주근깨 잡티 등의 색소 병변의 치료를 더 쉽게 하도록 도와줍니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">피부결 개선 및 피부톤 정돈 효과</div>
						<p class="box-cont">기존의 프락셔널 레이저 파장과 비교하면 흡수율이 약 10배가 높은 1927nm 파장을 사용합니다.</p>
					</li>
				</ul>
			</div>
			<div class="info-circle">
				<div class="info-title cont-title">미라클 레이저 시술범위</div>
				<ul class="list-wrap">
					<li class="list-item"><span class="txt">오타모반</span></li>
					<li class="list-item"><span class="txt">색소침착</span></li>
					<li class="list-item"><span class="txt">기미</span></li>
					<li class="list-item"><span class="txt">다양한 색소성트러블</span></li>
					<li class="list-item"><span class="txt">전반적인<br>피부상태 개선</span></li>
					<li class="list-item"><span class="txt">피부결 개선<br>탄력증진</span></li>
					<li class="list-item"><span class="txt">모공</span></li>
					<li class="list-item"><span class="txt">흉터</span></li>
				</ul>
			</div>
			
			<div class="qna-in-page">
				<div class="cont-title qna-title">자주묻는 질문</div>
				<dl class="cont-wrap">
					<dt class="question">일상생활하는데 지장은 없나요?</dt>
					<dd class="answer">미세한 딱지가 얼굴 전체에 생기기 때문에 처음에는 조금 어두워 졌다는 생각을 하실수도 있으나 피부화장을 통하여 커버가 가능하기 때문에 일상에 큰 지장은 없습니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">통증이 어느정도 인가요? 수면마취를 해야하나요?</dt>
					<dd class="answer">큰 통증은 없고 화끈거리고 뜨거운 느낌을 받으실수 있습니다. 보통은 마취크림을 바르고 진행하게 됩니다. 하지만 개인에 따라 통증이 다를 수도 있으니 수면마취 등은 전문가와 상의하시길 바랍니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">특별한 주의사항이 있나요?</dt>
					<dd class="answer">딱지가 완전히 떨어질 때 까지 관리가 매우 중요합니다. 그 과정에서 재생 앰플(MJ올 피부과 판매 중,  EGF성장인자 앰플) 또는, 재생크림등을 사용하여 빠른 재생을 돕고 딱지가 깔끔하게 떨어지도록 관리해야 합니다. 미라클 레이저 시술 후 피코 레이저 토닝이나 다른 화이트닝 관리 프로그램과 병행해서 프로그램으로 치료를 받을 때 더 좋은 효과를 볼 수 있습니다.</dd>
				</dl>
			</div>


		</section>



	</div>
	<!-- end : page content 영역 -->
	