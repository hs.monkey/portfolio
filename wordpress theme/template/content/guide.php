<!-- page content 영역 -->
<div id="content" class="content-wrap">

<div class="guide-item"><div class="guide-title">Guide</div>
<!-- 드래그 아래로 -->
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title">content-title</div>
<!-- 드래그 아래로 -->
	<section class="content-head">
		<h3 class="content-title">병원 갤러리</h3>
		<p class="title-suffix">MJ올피부과 내부를 사진으로 소개합니다.</p>
	</section>
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title">content-title + headcopy</div>
<!-- 드래그 아래로 -->
	<section class="content-head">
		<h3 class="content-title">인사말</h3>
		<p class="title-suffix w_line">MJ올피부과를 방문해주셔서 감사합니다.</p>
	</section>
	<section class="headcopy">
		<div class="tit">MJ올 피부과의원 – 올곧은 피부과</div>
		<p class="subtxt">
		장인이 최상의 옷감을 탄생시키기 위해 실의 한 올 한 올을 뒤틀림 없이 바르게 짜듯 MJ올 피부과에 오시는 모든 분들의 아름다운 피부를 위해 올바르고 정성을 다하는 마음으로 진료하고 치료하겠습니다.
		</p>
	</section>
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title">Info Box : 특징</div>
<!-- 드래그 아래로 -->
	<div class="info-box">
		<ul class="box-wrap">
			<li class="box-item">
				<div class="box-title">한번의 시술로 복합적 병변을 해결</div>
				<p class="box-cont">혈관, 색소 뿐 아니라 콜라겐에도 모두 효과적으로 작용하므로 복합적인 병변을 동시에 치료가 가능합니다.</p>
			</li>
			<li class="box-item">
				<div class="box-title">간편한 시술</div>
				<p class="box-cont">통증, 부작용, 다운타임 최소화통증이 거의 없어 일상생활에 지장이 없이 바로 가능한 간편한 레이저 시술입니다. 10~20분 이내의 치료시간으로 부담 없이 간편하게 시술 받으실 수 있습니다.</p>
			</li>
			<li class="box-item">
				<div class="box-title">안전한 시술</div>
				<p class="box-cont">통증이나 화상과 같은 레이저로 인한 피부의 손상을 줄여주는 안전하고 편안한 시술입니다.</p>
			</li>
		</ul>
	</div>
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title">Info Circle : 시술범위</div>
<!-- 드래그 아래로 -->
	<div class="info-circle">
		<div class="info-title cont-title">더마블레이트 시술범위</div>
		<ul class="list-wrap">
			<li class="list-item"><span class="txt">확장된 모공</span></li>
			<li class="list-item"><span class="txt">여드름 흉터 및 각종흉터</span></li>
			<li class="list-item"><span class="txt">주름</span></li>
			<li class="list-item"><span class="txt">튼살치료</span></li>
			<li class="list-item"><span class="txt">피지션 모반</span></li>
			<li class="list-item"><span class="txt">표피 모반</span></li>
			<li class="list-item"><span class="txt">지루성 각화증</span></li>
			<li class="list-item"><span class="txt">흑색증</span></li>
		</ul>
	</div>
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title">Info Text : 효과</div>
<!-- 드래그 아래로 -->
	<div class="info-text">
		<div class="infoText-title cont-title">더마블레이트 효과</div>
		<ul class="list-wrap">
			<li class="list-item">시술 시 콜라겐 생성으로 피부탄력에 효과를 줍니다.</li>
			<li class="list-item">흉터치료와 피부재생이 동시에 가능합니다.</li>
			<li class="list-item">세밀한 치료로 심한 부위에도 시술이 가능합니다.</li>
		</ul>
	</div>
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title">Info Check List : 시술 후 주의사항</div>
<!-- 드래그 아래로 -->
	<div class="info-checklist">
		<div class="checklist-title cont-title">시술 후 주의사항</div>
		<ul class="list-wrap">
			<li class="list-item">시술 부위가 붉게 보일 수 있습니다.</li>
			<li class="list-item">세안과 화장은 다음날부터 가능합니다.</li>
			<li class="list-item">일주일 간 사우나, 목욕탕은 피해 주세요.</li>
		</ul>
	</div>
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title">자주묻는 질문</div>
<!-- 드래그 아래로 -->
	<div class="qna-in-page">
		<div class="cont-title qna-title">자주묻는 질문</div>
		<dl class="cont-wrap">
			<dt class="question">시술 주기는 어떻게 되나요?</dt>
			<dd class="answer">환자의 피부타입이나 시술종류에 따라 차이가 있으나, 대략 2~3주 간격으로 3~5회 시술을 합니다.</dd>
		</dl>
		<dl class="cont-wrap">
			<dt class="question">부작용은 없나요?</dt>
			<dd class="answer">Long pulse mode 사용 시, 흉반이나 부종이 있으나 1~2일 안에 가라앉기 때문에 걱정하지 않으셔도 됩니다. 색소나 혈관치료 후 피부타입이나 병변에 따라 PIH, 수포, 지반(멍) 현상이 나타날 수도 있습니다.</dd>
		</dl>
		<dl class="cont-wrap">
			<dt class="question">시술 후 관리는 어떻게 하나요?</dt>
			<dd class="answer">레이저 시술 후에는 보습과 진정관리가 중요합니다. 또한 자외선은 광노화현상과 직접적인 피부손상을 유발하기 때문에 레이저 시술 후에는 반드시 자외선차단제를 사용해 주세요.</dd>
		</dl>
	</div>
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title">인포그라피</div>
<!-- 드래그 아래로 -->
	<div class="polygon-content section">
		<h4>WHY MJ올 피부과?</h4>
		<ul class="poly-item">
			<li>
			<dl>
				<dt>전문성</dt>
				<dd>MJ올 피부과 김문정 원장님이 피부에 바친 노력을 선사합니다.</dd>
			</dl>
			</li>
			<li>
			<dl>
				<dt>신뢰성</dt>
				<dd>오랜 노하우로 시술 만족도를 최대한 느끼실 수 있습니다.</dd>
			</dl>
			</li>
			<li>
			<dl>
				<dt>전문성</dt>
				<dd>MJ올 피부과 김문정 원장님이 피부에 바친 노력을 선사합니다.</dd>
			</dl>
			</li>
			<li>
			<dl>
				<dt>신뢰성</dt>
				<dd>오랜 노하우로 시술 만족도를 최대한 느끼실 수 있습니다.</dd>
			</dl>
			</li>
		</ul>
		<p>
			피부과는 정말 많이 있습니다. 하지만 어떤 것을 기준으로 선택해야할까요?
			<span class="block">단순 가격이나 과장광고를 통해 잘못된 선택을 하신다면 예뻐지고 나아지려 했다가 오히려 안좋은 결과를 얻으실 수 있습니다. </span>
			오랜 연구와 경험을 바탕으로 전문성을 갖고 진료하시는 김문정 원장님께당신의 소중한 피부를 맡겨보세요.
		</p>
	</div>
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title">box</div>
<!-- 드래그 아래로 -->
	<div class="line-box-wrap">
		<dl class="line-box top-title">
		<dt>진료과목</dt>
		<dd>
			<ul class="medical-subject">
			<li>
				<dl class="subject-list">
				<dt>피부질환 클리닉
				</dt>
				<dd>
					<ul>
					<li>알레르기성 접촉 피부염</li>
					<li>아토피</li>
					<li>건선</li>
					<li>백반증</li>
					<li>사마귀</li>
					<li>피부진균증</li>
					</ul>
				</dd>
				</dl>
			</li>
			<li>
				<dl class="subject-list">
				<dt>색소치료 클리닉
				</dt>
				<dd>
					<ul>
					<li>기미</li>
					<li>주근깨</li>
					<li>오타반점</li>
					<li>점</li>
					<li>노인반점</li>
					<li>밀크커피반점</li>
					</ul>
				</dd>
				</dl>
			</li>
			<li>
				<dl class="subject-list">
				<dt>안면윤곽 클리닉
				</dt>
				<dd>
					<ul>
					<li>필러</li>
					<li>자가혈 PRP이식</li>
					<li>미니 지방이식</li>
					<li>아큐스컬프</li>
					<li>안면조각술</li>
					<li>메조리프트</li>
					<li>쁘띠 성형</li>
					</ul>
				</dd>
				</dl>
			</li>
			<li>
				<ul class="subject-list">
				<li>안면홍조, 혈관확장증 치료 클리닉</li>
				<li>안티에이징 클리닉</li>
				<li>반영구화장 클리닉</li>
				<li>메디컬스킨케어</li>
				<li>바디케어</li>
				<li>탈모 클리닉</li>
				<li>제모 클리닉</li>
				</ul>
			</li>
			</ul>
		</dd>
		</dl>
	</div>
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title"></div>
<!-- 드래그 아래로 -->
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title"></div>
<!-- 드래그 아래로 -->
<!-- 드래그 여기까지 -->
</div>

<div class="guide-item"><div class="guide-title"></div>
<!-- 드래그 아래로 -->
<!-- 드래그 여기까지 -->
</div>



</div>
<!-- end : page content 영역 -->