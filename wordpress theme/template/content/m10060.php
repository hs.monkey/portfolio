	<!-- page content 영역 -->
	<div id="content" class="content-wrap whitening-dermablate">

		<section class="content-head">
			<h3 class="content-title">더마블레이트</h3>
			<p class="title-suffix w_line">MJ올피부과는 당신에게 건강한 아름다움을 선사합니다.</p>
		</section>
		<section class="headcopy">
			<div class="tit fc-main">정밀하고 섬세한 치료로 사과처럼 매끈한 피부로!</div>
			<p class="subtxt">
			독일 최첨단 광학 기술이 만들어 낸 신화 더마브레이트 사과 껍질도 섬세하게 깍아 낼 수 있을 정도로 정밀하고 섬세한 치료가 가능하며 사과처럼 매끈한 피부로 만들어 준다고 하여 애플레이저라고도 불립니다. 열 손상을 통해 피부재생을 유도하는 기존의 치료방식이 아닌 피부에 열손상을 주지 않고 진피층까지 자극을 주어 새로운 조직 생성을 유도합니다.
			</p>
		</section>


		<section class="content">
			<h3 class="blind">content</h3>

			<div class="info-box">
				<ul class="box-wrap">
					<li class="box-item">
						<div class="box-title">안전성</div>
						<p class="box-cont">열 손상 없이 1㎜ 이상의 피부 진피층까지 깊숙이 침투하여 피부 재생을 유도합니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">탁월한 효과</div>
						<p class="box-cont">100um 이하의 가장 미세한 자극으로 정교하고 세밀한 시술이 가능합니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">마이크로 단위의 정밀도</div>
						<p class="box-cont">현재 프락셔널 시술법 중 가장 최첨단으로 가장 정밀한 치료가 가능합니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">빠른 회복기간</div>
						<p class="box-cont">세계 유일의 마이크로 펄스 방식으로 치료효과가 높고 통증이 적습니다.</p>
					</li>
				</ul>
			</div>

			<div class="info-circle">
				<div class="info-title cont-title">더마블레이트 시술범위</div>
				<ul class="list-wrap">
					<li class="list-item"><span class="txt">확장된 모공</span></li>
					<li class="list-item"><span class="txt">여드름 흉터 및 각종흉터</span></li>
					<li class="list-item"><span class="txt">주름</span></li>
					<li class="list-item"><span class="txt">튼살치료</span></li>
					<li class="list-item"><span class="txt">피지션 모반</span></li>
					<li class="list-item"><span class="txt">표피 모반</span></li>
					<li class="list-item"><span class="txt">지루성 각화증</span></li>
					<li class="list-item"><span class="txt">흑색증</span></li>
				</ul>
			</div>

			<div class="product-img">
				<img src="<?php echo do_shortcode('[path]') ?>/images/content/product/dermablate.jpg" alt="더마블레이트">
				<param name="test" value="speedy">
			</div>

			<div class="info-text">
				<div class="infoText-title cont-title">더마블레이트 효과</div>
				<ul class="list-wrap">
					<li class="list-item">시술 시 콜라겐 생성으로 피부탄력에 효과를 줍니다.</li>
					<li class="list-item">흉터치료와 피부재생이 동시에 가능합니다.</li>
					<li class="list-item">세밀한 치료로 심한 부위에도 시술이 가능합니다.</li>
				</ul>
			</div>

			<div class="info-checklist">
				<div class="checklist-title cont-title">시술 후 주의사항</div>
				<ul class="list-wrap">
					<li class="list-item">시술 부위가 붉게 보일 수 있습니다.</li>
					<li class="list-item">세안과 화장은 다음날부터 가능합니다.</li>
					<li class="list-item">일주일 간 사우나, 목욕탕은 피해 주세요.</li>
				</ul>
			</div>

		</section>



	</div>
	<!-- end : page content 영역 -->
	