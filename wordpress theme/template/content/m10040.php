	<!-- page content 영역 -->
	<div id="content" class="content-wrap whitening-gv">

		<section class="content-head">
			<h3 class="content-title">GV레이저</h3>
			<p class="title-suffix w_line">MJ올피부과는 당신에게 건강한 아름다움을 선사합니다.</p>
		</section>
		<section class="headcopy">
			<div class="tit fc-main">색소 뿐만 아니라 혈관, 모공, 탄력 효과까지 내 피부의 모든 문제를 동시에 해결!</div>
			<p class="subtxt">
			독일 최첨단 광학기술로 만든 GV레이저는 혈관, 색소, 모공, 탄력 등 피부 전반에 걸친 다양한 문제들을 
			최적의 방식으로 치료하는 토탈 솔루션 레이저 시술입니다.
			</p>
		</section>

		<section class="content">
			<h3 class="blind">content</h3>

			<div class="info-box">
				<ul class="box-wrap">
					<li class="box-item">
						<div class="box-title">한번의 시술로 복합적 병변을 해결</div>
						<p class="box-cont">혈관, 색소 뿐 아니라 콜라겐에도 모두 효과적으로 작용하므로 복합적인 병변을 동시에 치료가 가능합니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">간편한 시술</div>
						<p class="box-cont">통증, 부작용, 다운타임 최소화통증이 거의 없어 일상생활에 지장이 없이 바로 가능한 간편한 레이저 시술입니다. 10~20분 이내의 치료시간으로 부담 없이 간편하게 시술 받으실 수 있습니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">안전한 시술</div>
						<p class="box-cont">통증이나 화상과 같은 레이저로 인한 피부의 손상을 줄여주는 안전하고 편안한 시술입니다.</p>
					</li>
				</ul>
			</div>

			<div class="product-img">
				<img src="<?php echo do_shortcode('[path]') ?>/images/content/product/gv.jpg" alt="GV 레이저">
				<param name="test" value="speedy">
			</div>

			<div class="qna-in-page">
				<div class="qna-title cont-title">자주묻는 질문</div>
				<dl class="cont-wrap">
					<dt class="question">시술 주기는 어떻게 되나요?</dt>
					<dd class="answer">환자의 피부타입이나 시술종류에 따라 차이가 있으나, 대략 2~3주 간격으로 3~5회 시술을 합니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">부작용은 없나요?</dt>
					<dd class="answer">Long pulse mode 사용 시, 흉반이나 부종이 있으나 1~2일 안에 가라앉기 때문에 걱정하지 않으셔도 됩니다. 색소나 혈관치료 후 피부타입이나 병변에 따라 PIH, 수포, 지반(멍) 현상이 나타날 수도 있습니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">시술 후 관리는 어떻게 하나요?</dt>
					<dd class="answer">레이저 시술 후에는 보습과 진정관리가 중요합니다. 또한 자외선은 광노화현상과 직접적인 피부손상을 유발하기 때문에 레이저 시술 후에는 반드시 자외선차단제를 사용해 주세요.</dd>
				</dl>
			</div>

		</section>



	</div>
	<!-- end : page content 영역 -->
	