	<!-- page content 영역 -->
	<div id="content" class="content-wrap whitening-starux">

		<section class="content-head">
			<h3 class="content-title">스타룩스</h3>
			<p class="title-suffix w_line">MJ올피부과는 당신에게 건강한 아름다움을 선사합니다.</p>
		</section>
		<section class="headcopy">
			<div class="tit fc-main">고품격 프랙셔널 레이저</div>
			<p class="subtxt">
			스타룩스XD는 기존의 프락셔널 레이저보다 업그레이드 된 고품격 프랙셔널 레이저입니다. 피부표면을 안전하게 보호하면서 피부 깊숙이 열에너지를 전달하여 재생을 유도하며 피지등으로 인해 벌어진 모공, 여드름으로 인해 생긴 흉터, 눈가의 잔주름, 피부탄력, 기미등을 개선하는데 효과적인 레이저입니다.
			</p>
		</section>

		<section class="content">
			<h3 class="blind">content</h3>

			<div class="info-box">
				<ul class="box-wrap">
					<li class="box-item">
						<div class="box-title">깊은 침투</div>
						<p class="box-cont">다른 레이저나 RF시술에 비하여 레이저가 더욱 깊숙이 침투하기 때문에 모공,흉터,잔주름 등이 빠르고 효과적으로 개선됩니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">부위별 강도 조절</div>
						<p class="box-cont">위부분은 손상을 적게 입히고 콜라겐 재생이 이루어지는 아래부분은 많이 자극하여 치료효과를 높여줍니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">낮은 시술 부담감</div>
						<p class="box-cont">일상생활에 거의 지장이 없습니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">약한 통증</div>
						<p class="box-cont">아이스팩등의 쿨링처치만으로 약간의 후끈거림 외에 크게 통증을 느끼지 못합니다.</p>
					</li>
				</ul>
			</div>

			<div class="info-circle">
				<div class="info-title cont-title">스타룩스 시술범위</div>
				<ul class="list-wrap">
					<li class="list-item"><span class="txt">벌어진 모공</span></li>
					<li class="list-item"><span class="txt">여드름 흉터</span></li>
					<li class="list-item"><span class="txt">눈가 잔주름</span></li>
					<li class="list-item"><span class="txt">피부탄력</span></li>
					<li class="list-item"><span class="txt">기미</span></li>
				</ul>
			</div>

			<div class="product-img">
				<img src="<?php echo do_shortcode('[path]') ?>/images/content/product/starux.jpg" alt="스타룩스">
				<param name="test" value="speedy">
			</div>

			<div class="info-text">
				<div class="infoText-title cont-title">스타룩스 효과</div>
				<ul class="list-wrap">
					<li class="list-item">스타룩스XD는 기존의 프락셔널 레이저보다 업그레이드 된 고품격 프랙셔널 레이저</li>
					<li class="list-item">피부표면을 안전하게 보호하면서 피부 깊숙이 열에너지를 전달하여 재생을 유도합니다.</li>
					<li class="list-item">피지등으로 인해 벌어진 모공, 여드름으로 인해 생긴 흉터, 눈가의 잔주름, 피부탄력, 기미등을 개선하는데 효과적인 레이저입니다.</li>
				</ul>
			</div>

		</section>



	</div>
	<!-- end : page content 영역 -->
	