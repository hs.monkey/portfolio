	<!-- page content 영역 -->
	<div id="content" class="content-wrap whitening-co2">

		<section class="content-head">
			<h3 class="content-title">탄산가스 레이저</h3>
			<p class="title-suffix w_line">MJ올피부과는 당신에게 건강한 아름다움을 선사합니다.</p>
		</section>
		<section class="headcopy">
			<div class="tit fc-main">정교한 시술을 요하는 다양한 피부 질환 치료</div>
			<p class="subtxt">
				이산화탄소 레이져는 원하는 피부 병변을 정확이 원하는 깊이 만큼씩 파괴할 수 있기 때문에 정교한 시술을 요하는 다양한 피부 질환 치료에 사용되고 있습니다. 10600nm의 파장으로 반사나 산란이 적어 정확한 위치에 레이저를 조사할 수 있으며, 펄스가 짧아 다른 부위에 손상이 적습니다. 또한 최근에 개발된 슈퍼 펄스 이산화탄소 레이저는 정상조직의 파괴를 줄이고 병변 조직만을 파괴할 수 있도록 안전하게 고안되었습니다.
			</p>
		</section>

		<section class="content">
			<h3 class="blind">content</h3>

			<div class="info-box">
				<ul class="box-wrap">
					<li class="box-item">
						<div class="box-title">안전성</div>
						<p class="box-cont">피부과에서 가장 광범위하게 사용되는 레이저로 흔히 점을 뺄 때 사용합니다. 10,600nm의 파장을 이용하므로 물에 잘 흡수되어 수분이 많은 피부조직에 잘 반응합니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">정밀함</div>
						<p class="box-cont">반사나 산란이 적어 표적에 대부분의 에너지를 집중시킬 수 있고, 펄스를 매우 짧게 하여 표적 이외의 주위 조직에는 열 손상을 최소화 합니다.</p>
					</li>
				</ul>
			</div>

			<div class="info-circle">
				<div class="info-title cont-title">탄산가스 레이저 시술범위</div>
				<ul class="list-wrap">
					<li class="list-item"><span class="txt">레이저 박피술</span></li>
					<li class="list-item"><span class="txt">안면주름 제거</span></li>
					<li class="list-item"><span class="txt">수두 및 마마 자국제거</span></li>
					<li class="list-item"><span class="txt">점, 주근깨,<br>검버섯</span></li>
					<li class="list-item"><span class="txt">표피모반</span></li>
					<li class="list-item"><span class="txt">여드름 흉터</span></li>
					<li class="list-item"><span class="txt">수술/외상 후 발생한 흉터</span></li>
					<li class="list-item"><span class="txt">쥐젖, 사마귀,<br>티눈</span></li>
				</ul>
			</div>

		</section>



	</div>
	<!-- end : page content 영역 -->
	