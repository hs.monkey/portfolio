<!-- page content 영역 -->
<div id="content" class="content-wrap">

  <section class="content-head">
    <h3 class="content-title">진료시간/오시는길</h3>
    <p class="title-suffix">안전하고 편안한 진료를 위해 항상 최선을 다하겠습니다.</p>

  </section>

  <section class="content">
    <h3 class="blind">content</h3>

    <div class="consultation-hours">
      <dl>
        <dt>진료시간</dt>
        <dd>
          <ul class="time-list">
            <li>
              <i>평　일</i><span>AM 10 : 00 ~ PM 20 : 00</span>
            </li>
            <li>
              <i>토요일</i><span>AM 09 : 30 ~ PM 16 : 00</span>
            </li>
            <li>
              <i>목요일</i><span>휴진</span>
            </li>
          </ul>
        </dd>
      </dl>
    </div>

  <div class="line-box-wrap">
    <dl class="line-box top-title">
      <dt>진료과목</dt>
      <dd>
        <ul class="medical-subject">
          <li>
            <dl class="subject-list">
              <dt>피부질환 클리닉
              </dt>
              <dd>
                <ul>
                  <li>알레르기성 접촉 피부염</li>
                  <li>아토피</li>
                  <li>건선</li>
                  <li>백반증</li>
                  <li>사마귀</li>
                  <li>피부진균증</li>
                </ul>
              </dd>
            </dl>
          </li>
          <li>
            <dl class="subject-list">
              <dt>색소치료 클리닉
              </dt>
              <dd>
                <ul>
                  <li>기미</li>
                  <li>주근깨</li>
                  <li>오타반점</li>
                  <li>점</li>
                  <li>노인반점</li>
                  <li>밀크커피반점</li>
                </ul>
              </dd>
            </dl>
          </li>
          <li>
            <dl class="subject-list">
              <dt>안면윤곽 클리닉
              </dt>
              <dd>
                <ul>
                  <li>필러</li>
                  <li>자가혈 PRP이식</li>
                  <li>미니 지방이식</li>
                  <li>아큐스컬프</li>
                  <li>안면조각술</li>
                  <li>메조리프트</li>
                  <li>쁘띠 성형</li>
                </ul>
              </dd>
            </dl>
          </li>
          <li>
            <ul class="subject-list">
              <li>안면홍조, 혈관확장증 치료 클리닉</li>
              <li>안티에이징 클리닉</li>
              <li>반영구화장 클리닉</li>
              <li>메디컬스킨케어</li>
              <li>바디케어</li>
              <li>탈모 클리닉</li>
              <li>제모 클리닉</li>
            </ul>
          </li>
        </ul>
      </dd>
    </dl>
  </div>

    <div class="location">
      <div class="textimg-wrap">
        <img src="<?php echo do_shortcode('[path]') ?>/images/content/img-loca.png" alt="오시는 길">
        <a href="<?php echo do_shortcode('[path]') ?>/images/content/img-loca.png" target="_blank" title="원본크기 보기"></a>
      </div>
      <dl>
        <dt>오시는 길</dt>
        <dd>
          <ul class="contact-list">
            <li>
              <dl>
                <dt>MJ올 피부과</dt>
                <dd>
                  서울특별시 중구 명동8가길 27 (충무로 2가 11-1) 선샤인빌딩 5층
                </dd>
              </dl>
            </li>
            <li>
              <dl>
                <dt>지하철 이용시</dt>
                <dd>
                  명동역 9번출구에서 우리은행 방향으로 145m
                </dd>
              </dl>
            </li>
            <li>
              <dl>
                <dt>상담 및 진료예약</dt>
                <dd class="telnum">
                  <a href="tel:02-777-8275" class="link-tel">02-777-8275</a>
                </dd>
              </dl>
            </li>
          </ul>
        </dd>
      </dl>
    </div>
  </section>



</div>
<!-- end : page content 영역 -->