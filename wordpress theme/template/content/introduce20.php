<!-- page content 영역 -->
<div id="content" class="content-wrap">

  <section class="content-head">
    <h3 class="content-title">병원 갤러리</h3>
    <p class="title-suffix">MJ올피부과 내부를 사진으로 소개합니다.</p>
  </section>

  <section class="content">
    <h3 class="blind">content</h3>
    <ul class="gallery-list slider-for">
    <li>
        <span class="img-area"><!-- img 호출 --></span>
        <div class="slide-text-box">
          <p>
          </p>
        </div>
      </li>
      <li>
        <span class="img-area"><!-- img 호출 --></span>
        <div class="slide-text-box">
          <p>
          </p>
        </div>
      </li>
      <li>
        <span class="img-area"><!-- img 호출 --></span>
        <div class="slide-text-box">
          <p>
          </p>
        </div>
      </li>
      <li>
        <span class="img-area"><!-- img 호출 --></span>
        <div class="slide-text-box">
          <p>
          </p>
        </div>
      </li>
      <li>
        <span class="img-area"><!-- img 호출 --></span>
        <div class="slide-text-box">
          <p>
          </p>
        </div>
      </li>
      <li>
        <span class="img-area"><!-- img 호출 --></span>
        <div class="slide-text-box">
          <p>
          </p>
        </div>
      </li>
      <li>
        <span class="img-area"><!-- img 호출 --></span>
        <div class="slide-text-box">
          <p>
          </p>
        </div>
      </li>
      <li>
        <span class="img-area"><!-- img 호출 --></span>
        <div class="slide-text-box">
          <p>
          </p>
        </div>
      </li>
    </ul>
    <ul class="slider-nav">
      <li>
        <img src="<?php echo do_shortcode('[path]') ?>/images/content/bg-gall00.jpg" alt="상담실">
        <div class="slide-hover-text">
          <div class="hover-text-wrap">
            
          </div>
        </div>
      </li>
      <li>
      <img src="<?php echo do_shortcode('[path]') ?>/images/content/bg-gall01.jpg" alt="인포메이션">
      <div class="slide-hover-text">
          <div class="hover-text-wrap">
            
          </div>
        </div>
      </li>
      <li>
        <img src="<?php echo do_shortcode('[path]') ?>/images/content/bg-gall02.jpg" alt="진료대기실">
        <div class="slide-hover-text">
          <div class="hover-text-wrap">
            
          </div>
        </div>
      </li>
      <li>
        <img src="<?php echo do_shortcode('[path]') ?>/images/content/bg-gall03.jpg" alt="시술실">
        <div class="slide-hover-text">
          <div class="hover-text-wrap">
            
          </div>
        </div>
      </li>
      <li>
        <img src="<?php echo do_shortcode('[path]') ?>/images/content/bg-gall04.jpg" alt="파우더룸">
        <div class="slide-hover-text">
          <div class="hover-text-wrap">
            
          </div>
        </div>
      </li>
      <li>
        <img src="<?php echo do_shortcode('[path]') ?>/images/content/bg-gall05.jpg" alt="VIP룸">
        <div class="slide-hover-text">
          <div class="hover-text-wrap">
            
          </div>
        </div>
      </li>
      <li>
        <img src="<?php echo do_shortcode('[path]') ?>/images/content/bg-gall06.jpg" alt="시술실">
        <div class="slide-hover-text">
          <div class="hover-text-wrap">
            
          </div>
        </div>
      </li>
      <li>
        <img src="<?php echo do_shortcode('[path]') ?>/images/content/bg-gall07.jpg" alt="원장실">
        <div class="slide-hover-text">
          <div class="hover-text-wrap">
            
          </div>
        </div>
      </li>
    </ul>
  </section>
</div>

<script>
$(function(){

  // 병원갤러리 기능 추가 : 마무스오버, 코드 호출
  var counter_i = 0;
  while(  $('.slider-nav li:eq('+counter_i+')').length ){
    var temp1 = $('.gallery-list li:eq('+counter_i+')');
    var temp = $('.slider-nav li:eq('+counter_i+')');
    var alttext = temp.children('img').attr('alt');
    var img_code = temp.children('img').clone();
    temp1.find('.img-area').html(img_code);
    temp1.children('.slide-text-box').children('p').append('<span>'+alttext+'</span>');
    temp.children('.slide-hover-text').children('.hover-text-wrap').append('<span>'+alttext+'</span>');
    counter_i++;
  }

  $('.gallery-list').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.slider-nav',
  });

  $('.slider-nav').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    arrows: false,
    centerPadding: '0px',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          centerPadding: '20px',
          slidesToShow: 5
        }
      },
      {
        breakpoint: 480,
        settings: {
          centerMode: true,
          centerPadding: '10px',
          slidesToShow: 3
        }
      }
    ]
  });

});
</script>

<!-- end : page content 영역 -->