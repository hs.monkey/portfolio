<!-- page content 영역 -->
<div id="content" class="content-wrap">

  <section class="content-head">
    <h3 class="content-title">인사말</h3>
    <p class="title-suffix w_line">MJ올피부과를 방문해주셔서 감사합니다.</p>
  </section>
  <section class="headcopy">
    <div class="tit">MJ올 피부과의원 – 올곧은 피부과</div>
    <p class="subtxt">
      장인이 최상의 옷감을 탄생시키기 위해 실의 한 올 한 올을 뒤틀림 없이 바르게 짜듯 MJ올 피부과에 오시는 모든 분들의 아름다운 피부를 위해 올바르고 정성을 다하는 마음으로 진료하고 치료하겠습니다.
    </p>
  </section>

  <section class="content">
    <h3 class="blind">content</h3>

    <div class="bg-text section">
      <span class="bg-for-right">
        <img src="<?php echo do_shortcode('[path]') ?>/images/content/drkim.png" alt="김문정 원장">
      </span>
      <h4>
        안녕하세요? <span class="block">MJ올 피부과 전문의</span> 김문정 원장입니다.
      </h4>
      <p class="test">
        시대에 따라 미인에 대한 기준은 바뀌어왔습니다. 그러나 변치 않는 아름다움에서 빠질 수 없는 요소는 맑고 건강한 피부, 젊고 탄력 있는 피부입니다. MJ올 피부과는 당신의 건강한 피부를 위한 모든 것, 건강한 아름다움을
        위한 모든 것 위해 늘 최선을 다해 연구하고 노력할 것을 약속 드립니다.
      </p>
      <p>
        사춘기 시절 시작된 여드름과 여드름 흉터, 그로 인해 심해진 안면 홍조증, 어린 시절부터 갖고 있던 주근깨와 습진, 20대 중반부터 생겨난 피부 묘기성 두드러기, 제겐 이런 피부문제들이 너무나도 심각한 고민거리였습니다.
        이런 피부 문제들을 치료하기 위해 피부과 전문의 길을 택하였습니다.
      </p>
      <p>
        어린 시절 좋지 않은 피부를 가졌던 것이 이젠 제겐 축복입니다. 많은 환자분들을 더 잘 이해할 수 있고, 공부하고 연구하며 제가 받아본 다양한 시술 들을 각 환자분들의 피부타입에 맞게 맞춤형 치료를 해드릴 수 있기 때문입니다.
        또한 제 자신이 꾸준한 피부과 치료를 받아 지금은 비교적 건강한 피부를 가질 수 있게 되었기 때문입니다.”
      </p>
    </div>
    <div class="polygon-content section">
      <h4>WHY MJ올 피부과?</h4>
      <ul class="poly-item">
        <li>
          <dl>
            <dt>전문성</dt>
            <dd>MJ올 피부과 김문정 원장님이 피부에 바친 노력을 선사합니다.</dd>
          </dl>
        </li>
        <li>
          <dl>
            <dt>신뢰성</dt>
            <dd>오랜 노하우로 시술 만족도를 최대한 느끼실 수 있습니다.</dd>
          </dl>
        </li>
        <li>
          <dl>
            <dt>전문성</dt>
            <dd>MJ올 피부과 김문정 원장님이 피부에 바친 노력을 선사합니다.</dd>
          </dl>
        </li>
        <li>
          <dl>
            <dt>신뢰성</dt>
            <dd>오랜 노하우로 시술 만족도를 최대한 느끼실 수 있습니다.</dd>
          </dl>
        </li>
      </ul>
      <p>
        피부과는 정말 많이 있습니다. 하지만 어떤 것을 기준으로 선택해야할까요?
        <span class="block">단순 가격이나 과장광고를 통해 잘못된 선택을 하신다면 예뻐지고 나아지려 했다가 오히려 안좋은 결과를 얻으실 수 있습니다. </span>
        오랜 연구와 경험을 바탕으로 전문성을 갖고 진료하시는 김문정 원장님께당신의 소중한 피부를 맡겨보세요.
      </p>
    </div>
  </section>
 


</div>
<!-- end : page content 영역 -->