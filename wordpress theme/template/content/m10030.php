	<!-- page content 영역 -->
	<div id="content" class="content-wrap whitening-pico">

		<section class="content-head">
			<h3 class="content-title">피코 레이저</h3>
			<p class="title-suffix w_line">MJ올피부과는 당신에게 건강한 아름다움을 선사합니다.</p>
		</section>
		<section class="headcopy">
			<div class="tit fc-main">여드름 흉터, 잔주름, 넓은 모공, 푸석푸석한 피부결, 어두칙칙한 피부톤 개선</div>
			<p class="subtxt">
				532nm와 1064nm 파장을 이용하여 표피 색소뿐만 아니라 색소 및 다양한 컬러 문신 제거가 가능하며, 치료 부위의 주변조직 손상없이 정밀한 치료가 가능하기 때문에 부작용은 적고, 신속하고 극대화된 치료 효과를 확인하실 수 있습니다.
			</p>
		</section>

		<section class="content">
			<h3 class="blind">content</h3>

			<div class="info-box">
				<ul class="box-wrap">
					<li class="box-item">
						<div class="box-title">다양한 적응증 치료</div>
						<p class="box-cont">표피색소, 진피색소, 다양한 컬러문신 뿐만 아니라 여드름 흉터, 잔주름, 넓은 모공 등 치료가 가능합니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">더욱 효과적이고 안전한 치료</div>
						<p class="box-cont">색소를 깨는 물리적인 힘은 강하게, 부작용을 초래하는 열작용은 적게 전달되어 색소 치료 및 문신제거를 더욱 효과적이고 안전하게 치료합니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">허가 인증 완료</div>
						<p class="box-cont">식품의약품안전처 MFDA, 미국 FDA와 유럽 CE를 획득하여 국내외적으로 안정성과 효염성을 인증 받은 레이저 치료기기입니다.</p>
					</li>
				</ul>
			</div>
			<div class="info-circle">
				<div class="info-title cont-title">피코 레이저 시술범위</div>
				<ul class="list-wrap">
					<li class="list-item"><span class="txt">문신치료</span></li>
					<li class="list-item"><span class="txt">주근깨</span></li>
					<li class="list-item"><span class="txt">오타모반</span></li>
					<li class="list-item"><span class="txt">잡티제거</span></li>
					<li class="list-item"><span class="txt">색소침착</span></li>
					<li class="list-item"><span class="txt">모공 및<br>여드름 흉터</span></li>
				</ul>
			</div>

			<div class="product-img">
				<img src="<?php echo do_shortcode('[path]') ?>/images/content/product/Picocare.png" alt="피코레이저">
			</div>

			<div class="info-text">
				<div class="infoText-title cont-title">피코 레이저 효과</div>
				<ul class="list-wrap">
					<li class="list-item">기존 레이저보다 1,000배 빠르게 레이저가 조사되어 색소를 정교하게 지료할 수 있습니다.</li>
					<li class="list-item">치료 부위의 주변조직 손상없이 정밀한 치료가 가능합니다.</li>
					<li class="list-item">여드름 흉터, 잔주름, 넓은 모공, 푸석푸석한 피부결, 어두칙칙한 피부톤 개선이 가능합니다.</li>
				</ul>
			</div>

			<div class="info-checklist">
				<div class="checklist-title cont-title">시술 후 주의사항</div>
				<ul class="list-wrap">
					<li class="list-item">시술 직후 흉반 및 열감이 있습니다. 비타민C 성분을 제외한 진정케어 또는 냉찜질을 권장합니다.</li>
					<li class="list-item">시술 부위에 수포, 가려움증 등이 발생할 수 있으며, 이는 2~3일 내 사라집니다.</li>
					<li class="list-item">붉은기가 사라지면 딱지가 형성됩니다. 인위적으로 떼지 않고 자연스럽게 떨어지오록 주의하세요.</li>
					<li class="list-item">시술 후에는 무자극성 또는 저자극성 재생크림, 보습제 사용을 권장하고, 특히 자외선 차단제 크림을 약 3~4시간 마다 사용할 것을 권장합니다.</li>
					<li class="list-item">시술 후 약 1주일 간 과한 운동, 사우나, 찜질방 이용 등은 삼가합니다.</li>
				</ul>
			</div>
			
			<div class="qna-in-page">
				<div class="cont-title qna-title">자주묻는 질문</div>
				<dl class="cont-wrap">
					<dt class="question">강력한 에너지를 사용하는데 피부통증은 심하지 않나요?</dt>
					<dd class="answer">경우에 따라 마취크림을 사용하기 때문에 통증에 대한 부담이 적습니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">시술 소요 시간은 얼마나 걸리나요?</dt>
					<dd class="answer">치료 부위와 크기에 따라 다르지만 보통 30분 이내로, 바쁜 직장인들은 점심시간을 활용하실 수 있습니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">효과는 언제부터 나타날까요?</dt>
					<dd class="answer">개인차가 있겠지만 1~2회 치료 후 부터 효과를 확인할 수 있습니다.</dd>
				</dl>
			</div>


		</section>



	</div>
	<!-- end : page content 영역 -->
	