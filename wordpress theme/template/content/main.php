<!-- page content 영역 -->
<div class="main_wrap">
	<section class="main_visual">
		<h1 class="blind">메인 상단 이미지</h1>
		<div class="main_slick">
			<div>
				<img src="<?php echo do_shortcode('[path]') ?>/images/main/bg-main01.jpg" alt="알트1">
			</div>
			<div>
				<img src="<?php echo do_shortcode('[path]') ?>/images/main/bg-main01.jpg" alt="알트1">
			</div>
			<div>
				<img src="<?php echo do_shortcode('[path]') ?>/images/main/bg-main01.jpg" alt="알트3">
			</div>
			<div>
				<img src="<?php echo do_shortcode('[path]') ?>/images/main/bg-main01.jpg" alt="알트4">
			</div>
		</div>
	</section>

	<section class="main_content">
		<h1 class="blind">바로가기 영역</h1>

		<div class="content-upper">

			<article class="shortcut_list">
				<section>
					<div class="wrap-bg box">
						<h2>화이트닝</h2>
						<p>당신의 빛나는 피부를 위한 MJ올피부과만의 특별한 시술!
							<span class="block"> 환한 피부로 아름다움을 되찾아보세요</span>
						</p>
						<a href="#;" title="화이트닝" target="_self" class="link">화이트닝 바로가기</a>
					</div>
				</section>
				<section>
					<div class="box">
						<h2>윤곽/리프팅</h2>
						<p>나이가 들면서 늘어지는 피부 무너지는 얼굴라인을
							<span class="block"> MJ올피부과가 돌려드리겠습니다.</span>
						</p>
						<a href="#;" title="화이트닝" target="_self" class="link">윤곽/리프팅 바로가기</a>
					</div>
				</section>
				<section>
					<div class="wrap-bg box">
						<h2>바디라인 케어</h2>
						<p>숨겨져있는 나만의 에스라인을 찾자! 아름다운 바디라인으로
							<span class="block"> 숨겨진 자신감을 되찾으세요.</p>
						<a href="#;" title="화이트닝" target="_self" class="link">바디라인 케어 바로가기</a>
					</div>
				</section>
				<section>
					<div class="box">
						<h2>쁘띠성형</h2>
						<p>각종 흉터나 튼살을 말끔하게 지워드립니다.
							<span class="block"> MJ올 피부과의 노하우로 당신의 고민을 해결해드리겠습니다.</span>
						</p>
						<a href="#;" title="화이트닝" target="_self" class="link">쁘띠성형 바로가기</a>
					</div>
				</section>
			</article>

			<div class="banner-section">
				<div class="main-dr">
					<h2>MJ올 피부과<span class="block">원장 김문정</span></h2>

					<p>김문정 원장만의<span class="block">노하우를 </span>피부로 느껴보세요</p>
					<a href="#;" title="소개페이지" class="color-button">자세히보기<span class="ico-arrow-right"></span></a>
				</div>
				<div class="main-banner">
					<div class="slide-item">
						<img src="<?php echo do_shortcode('[path]') ?>/images/main/bg-banner01-m.jpg" alt="알트1">
					</div>
					<div class="slide-item">
						<img src="<?php echo do_shortcode('[path]') ?>/images/main/bg-banner01-m.jpg" alt="알트2">
					</div>
					<div class="slide-item">
						<img src="<?php echo do_shortcode('[path]') ?>/images/main/bg-banner01-m.jpg" alt="알트3">
					</div>
				</div>
			</div>

		</div>

		<div class="story-section">
			<div class="story-inner">
				<div class="fr">
					<section class="story-content">
						<h2>
							MJ 올 피부과 스토리
						</h2>
						<p>MJ 올 피부과가 당신에게 하고 싶은 이야기를 들어보세요.</p>
						<a href="#;" title="스토리 페이지" class="color-button none-bg">자세히보기<span class="ico-arrow-right"></span></a>
					</section>
				</div>
			</div>
		</div>

	</section>
</div>

<script>
	$(function () {
		if ($('.main_wrap').length) {//메인페이지 구분클래스 삽입
			$('html').addClass('main');
		}

		$('.main_slick').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			fade: true,
			dots: true
		});

		$('.main-banner').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 2000,
			arrows: false,
			dots: true,
			
			responsive: [
				{
					breakpoint: 768,
					settings: {
						//dots: false
					}
				}
			]
		});
	});
</script>

<!-- end : page content 영역 -->