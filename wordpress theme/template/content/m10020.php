	<!-- page content 영역 -->
	<div id="content" class="content-wrap whitening-maxg">

		<section class="content-head">
			<h3 class="content-title">맥스지레이저</h3>
			<p class="title-suffix w_line">MJ올피부과는 당신에게 건강한 아름다움을 선사합니다.</p>
		</section>
		<section class="headcopy">
			<div class="tit fc-main">맥스 화이트닝으로 색소, 혈관, 탄력을 한번에!!</div>
			<p class="subtxt">
			나이가 들면서 거칠어진 피부나, 피부톤을 칙칙하게 보이게 하는 색소성 병변(기미, 잡티, 주근깨 등)과 혈관성 병변(홍조, 여드름 홍반, 늘어난 혈관 등)을 한번에 잡아줌으로써 경험 해 보지 못한 환한 피부를 가지게 됩니다. 각 병변에 효과적인 파장에 파워가 자동으로 설정되어 각 병변들을 빈틈없이 개선하기 때문에, 단 하나의 시술로 여러 가지 고민을 해결할 수 있습니다.
			</p>
		</section>

		<section class="content">
			<h3 class="blind">content</h3>

			<div class="info-box">
				<ul class="box-wrap">
					<li class="box-item">
						<div class="box-title">효과적인 파장의 파워가 자동으로</div>
						<p class="box-cont">MaxG의 Dynamic Spectrum Shifting라는 기술이 병변의 상태에 따라 자동으로 가장 과적인 파장에 힘을 실어줍니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">강력한 쿨링</div>
						<p class="box-cont">MaxG의 쿨링은 강력합니다. 5℃로 지속되는 쿨링 시스템으로 시술 시 표피를 보호하여 부작용을 없애고 더불어 통증까지 줄여줍니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">검증된 연구 결과</div>
						<p class="box-cont">모든 시술은 얼마만큼 그 효과와 안전성이 입증되었느냐가 중요합니다. 맥스 화이트닝(Max Whitening)은 미국 등에서 유명한 논문과 연구 결과로 이미 검증되었습니다. 안전하고 효과적인 술인 맥스 화이트닝(Max Whitening)은 믿고 받으셔도 됩니다.</p>
					</li>
				</ul>
			</div>

			<div class="product-img">
				<img src="<?php echo do_shortcode('[path]') ?>/images/content/product/whitenine_maxg.png" alt="맥스지레이저">
			</div>

			<div class="info-text">
				<div class="infoText-title cont-title">맥스지 효과</div>
				<ul class="list-wrap">
					<li class="list-item">피부탄력 거칠고, 힘없는 피부, 칙칙한 피부톤에 효과적입니다.</li>
					<li class="list-item">혈관성병변 홍조, 여드름홍반, 실핏줄에 효과적입니다.</li>
					<li class="list-item">색소병변 기미, 잡티, 주근깨, 검버섯에 효과적입니다.</li>
				</ul>
			</div>

			<div class="info-checklist">
				<div class="checklist-title cont-title">시술 후 주의사항</div>
				<ul class="list-wrap">
					<li class="list-item">시술 당일은 땀을 많이 흘리는 사우나나 격한 운동을 삼가하는게 좋습니다.</li>
					<li class="list-item">시술 후 약간의 각질 같은 딱지가 앉을 수도 있습니다<div class="span block"></div>이는 3~4일 후 자연적으로 떨어지게 되니 일부러 떼지 않도록 주의합니다.</li>
					<li class="list-item">시술 후 병원에서 처방 받은 재생크림, 보습제를 충분히 발라주면 회복에 도움이 되며 자외선 차단제는 딱지가 탈락된후 발라주는 것이 좋습니다.</li>
				</ul>
			</div>

			<div class="qna-in-page">
				<div class="cont-title qna-title">자주묻는 질문</div>
				<dl class="cont-wrap">
					<dt class="question">어떤 분에게 효과적인가요?</dt>
					<dd class="answer">나이가 들면서 피부가 거칠어지거나 피부 톤이 칙칙한 분들은 맑고 탱탱한 피부를 가질 수 있으며, 보기 싫은 잡티나 검버섯을 없애는 데도 매우 효과적입니다. 안면홍조나 여드름 홍반,모세혈관 확장증 등 혈관성병변을 갖고 있는 분들도 효과적이고 안전하게 치료받을 수 있습니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">시술시간은 얼마나 걸리나요?</dt>
					<dd class="answer">시술시간은 보통 15~20분 정도 소요되며, 온도에 영향을 받지 않기 때문에 계절이나 날씨에 제한 없이 시술 받으실 수 있습니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">몇 번 정도 시술 받아야 하나요?</dt>
					<dd class="answer">1~2회 시술만으로도 만족할 만한 결과를 기대할 수 있으나, 환자의 상태에 따라 3~4주 간격으로 3~5회 시술받는 것이 긍정의 효과를 얻어볼 수 있습니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">시술시 통증은 없나요?</dt>
					<dd class="answer">장비 자체에 강력한 냉각시스템이 있기 때문에 통증을 완화시키며 시술 시 표피까지 보호합니다.</dd>
				</dl>
			</div>


		</section>



	</div>
	<!-- end : page content 영역 -->
	