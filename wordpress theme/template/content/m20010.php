	<!-- page content 영역 -->
	<div id="content" class="content-wrap outline-laser">

		<section class="content-head">
			<h3 class="content-title">레이저 리프팅</h3>
			<p class="title-suffix w_line">MJ올피부과는 당신에게 건강한 아름다움을 선사합니다.</p>
		</section>
		<section class="headcopy">
			<div class="tit fc-main">화이트닝과 리프팅을 동시에!</div>
			<p class="subtxt">
			3G 토닝이란 Quasi-long-pulse라는 새로운 방식의 알렉산드라이트 레이저를 이용하여 기미 및 색소질환을 치료하는 Gentlemax 레이저를 이용한 신개념 치료입니다. 3G 토닝은 기존 기미 치료 레이저와는 달리 색소에 대한 흡수도가 높은 755nm 파장대의 레이저 빛을 이용해 멜라닌 색소 외의 주변피부에 불필요한 레이저 손상을 입히지 않고, 색소세포만을 선택적으로 제거하는 치료법입니다. 기존 Nd;Yag레이저 토닝보다 훨씬 안전한 출력의 에너지를 사용하기 때문에 색소의 공장인 멜라닌 색소세포에 대한 손상을 최소화해 과색소 침착이나 백반증과 같은 불필요한 부작용의 위험을 피할 수 있는 효과적이며 안전한 치료법입니다.
			</p>
		</section>

		<section class="content">
			<h3 class="blind">content</h3>

			<div class="info-box">
				<ul class="box-wrap">
					<li class="box-item">
						<div class="box-title">듀얼 모드의 레이저빔</div>
						<p class="box-cont">755nm alexandrite레이저와 1064nm Nd;Yag레이저를 혼합 채택으로 거의 모든 미용 시술 처리가 가능합니다.</p>
					</li>
					<li class="box-item">
						<div class="box-title">정밀한 시술과 빠른 시술 속도</div>
						<p class="box-cont">1.5~18㎜의 다양한 spot size 선택이 가능하고, 18㎜에서도 2Hz의 빠른시술속도로 시술시간이 짧습니다.</p>
					</li>
				</ul>
			</div>

			<div class="info-circle">
				<div class="info-title cont-title">젠틀맥스 레이저 시술범위</div>
				<ul class="list-wrap">
					<li class="list-item"><span class="txt">기미, 검버섯 등 색소성 질환</span></li>
					<li class="list-item"><span class="txt">피부탄력, 리프팅</span></li>
					<li class="list-item"><span class="txt">혈관성 질환</span></li>
					<li class="list-item"><span class="txt">제모</span></li>
				</ul>
			</div>

			<div class="product-img">
				<img src="<?php echo do_shortcode('[path]') ?>/images/content/product/gentlemax.jpg" alt="젠틀맥스 레이저">
				<param name="test" value="speedy">
			</div>

			<div class="info-text">
				<div class="infoText-title cont-title">젠틀맥스 레이저 효과</div>
				<ul class="list-wrap">
					<li class="list-item">Gentle Max는 레이저 토닝과 제네시스 리프팅을 동시에 할 수 있는 3G Whitening 시술법입니다.</li>
					<li class="list-item">따로따로 시술할 필요 없이 한 번에 피부 탄력과 색소성 질환을 개선합니다.</li>
					<li class="list-item">IPL이 치료하지 못했던 어려운 색소질환의 치료에도 효과가 있습니다.</li>
				</ul>
			</div>

			<div class="info-checklist">
				<div class="checklist-title cont-title">시술 후 주의사항</div>
				<ul class="list-wrap">
					<li class="list-item">시술 후에는 30분정도 화끈거리는 열감이 느껴지실 수 있으나 진정 관리 시술 후 차차 나아집니다. 재생크림을 열심히 바르면 재생에 큰 도움이 됩니다.</li>
					<li class="list-item">색소 주변으로 약간의 붉은기가 생길 수 있으나 1-2일 후면 없어지므로 너무 염려하지 않으셔도 됩니다.</li>
					<li class="list-item">시술 후 딱지가 완벽히 떨어질 때 까지 한증막이나 사우나는 피해 주시는 것이 좋습니다.</li>
					<li class="list-item">색소 부위 치료후 생긴 딱지는 7~14일 내에 서서히 떨어져 나갑니다. 억지로 떼어내거나 문지르는 등의 자극을 주지 않는 것이 좋습니다. 불가피하게 화장을 해야 하는 경우가 아니면 가급적 2~3일은 물로만 가볍게 세안하세요.</li>
					<li class="list-item">시술 부위의 보습에 신경을 써주는 것이 좋으며 병원에서 처방 받으신 재생 크림을 치료 직후부터 사용하시고 자외선 차단제는 7일 후부터 발라 자외선으로 인한 피부에 자극이 가지 않도록 해주는 것이 좋습니다.</li>
					<li class="list-item">가능하면 시술 후 일주일 내로 1회~2회정도 재생 LED치료를 받으십시오.</li>
				</ul>
			</div>

			<div class="qna-in-page">
				<div class="cont-title qna-title">자주묻는 질문</div>
				<dl class="cont-wrap">
					<dt class="question">시술시간은 얼마나 걸리나요?</dt>
					<dd class="answer">받는 시술의 종류에 따라 다르지만, 3G Whitening의 경우 5~10분, 다른 시술의 경우에는 15분안에 끝납니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">많이 아픈가요?</dt>
					<dd class="answer">걱정마세요! 자체냉각시스템이 있어 통증을 완화시키며, 따뜻한 열감이 있는 정도이기 때문에 별도의 마취가 필요 없을 정도로 아프지 않은 시술입니다.</dd>
				</dl>
				<dl class="cont-wrap">
					<dt class="question">3G Whitening은 몇 번의 시술이 필요한가요?</dt>
					<dd class="answer">평균 1~2회 만으로 만족할 만한 결과를 기대할 수 있으나 환자의 상태에 따라 3~4회 정도를 하는 경우도 있습니다. 보통 1회 시술 시 얼굴이 반짝반짝 윤이 나는 것을 느낄 수 있습니다.</dd>
				</dl>
			</div>


		</section>



	</div>
	<!-- end : page content 영역 -->
	