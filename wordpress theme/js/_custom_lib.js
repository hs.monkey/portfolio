// string formatter
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
                ? args[number]
                : match
            ;
        });
    };
}

var _common = {
    cancelPrevent: function(ev) {
        if (typeof ev === 'undefined') {
            return;
        }

        if (ev.preventDefault()) {
            ev.preventDefault();
        } else {
            ev.returnValue = false;
        }

    }
    ,setSelectVal: function(select, value) {
        Array.prototype.forEach.call(select.children,function(el){if(el.value==value.toString()){el.selected=true}});
    }
    ,cutByLen: function(s, mb) {
        if (!s || s.length < 1) {
            return '';
        }
        for(b=i=0;c=s.charCodeAt(i);) {
            b+=c>>7?2:1;
            if (b > mb)
                break;
            i++;
        }
        return s.substring(0,i);
    }
    ,isNum: function(n) {
        var _r = /[^0-9]/g;
        if ( _r.test(n) ) {
            return false;
        }
        return true;
    }
    ,getByLen: function(s, b) {
        if (!s|| s.length < 1) {
            return '';
        }
        for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
        return b;
    }
    ,getParams: function() {
        var _r = /(\?|&)(?=([^=]+)=([^&]+))/g;
        var params = {};
        while( match = _r.exec(window.location.href) ) {
            params[match[2]] = match[3];
        }

        return params;
    }
    ,nl2br: function(str) {
        return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
    }
    ,hasKey: function(_s, _t) {
        if (typeof _t === 'undefined') {
            return false;
        }

        for( var i=0, e=_s.length; i<e; i++ ) {
            if (!_t.hasOwnProperty(_s[i])) {
                return false;
            }
        }

        return true;
    }
    ,removeChilds: function(t) {
        while( t.hasChildNodes() ) {
            t.removeChild(t.firstChild);
        }
    }
    ,removeMarkup: function(s) {
        return s.replace(/<\/?[^>]+(>|$)/g, '');
    }
    ,newEl: function(t, o) {
        var el = document.createElement(t);
        if ( o === null ) {
            return el;
        }

        for (key in o) {
            el.setAttribute(key, o[key]);
        }
        return el;
    }
    ,pagination: function(curPage, limit, founds) {
        if (curPage < 1) {
            curPage = 1;
        }
        if (limit < 1) {
            limit = 1;
        }
        if (founds < 1) {
            founds = 0;
        }

        var totalPage = (founds - 1) / limit + 1;
        var startPage = Math.floor((curPage - 1) / limit) * limit + 1;
        var endPage = startPage + (limit - 1);
        var prev = 1;
        var next = endPage + 1;

        if (endPage > totalPage) {
            endPage = totalPage;
        }

        if (endPage < 1) {
            endPage = 1;
        }

        if (startPage - 10 > 0) {
            prev = startPage - 10;
        }

        if (next > totalPage) {
            next = totalPage;
        }

        return {
            totalPage: parseInt(totalPage)
            ,startPage: parseInt(startPage)
            ,endPage: parseInt(endPage)
            ,curPage: parseInt(curPage)
            ,prev: parseInt(prev)
            ,next: parseInt(next)
        }

    }
}
