$(function () {// common	

//	todo 개발시 삭제 --pc화면에서 개발자 도구로 모바일 화면을 볼 때만 사용합니다.	
	var fn_reWinW = function(){
		if( $(window).width()>1366 ) {
			$('html').removeClass('mob').addClass('pc');
		}else{
			$('html').removeClass('pc').addClass('mob');
		}
	}
	fn_reWinW();
	$(window).resize(function(){
		fn_reWinW();
	});
	//---/---
	
	//텝 메뉴
	if ($('.tabs').length){
		$('.tabs .btn_tab').on('click', function (e) {
			$(this).parent('li').addClass('active').siblings().removeClass('active');
		});
		if ($('.tabContents').length){
			$('.tabs .btn_tab[href*="tabCont-"]').on('click', function (e) {
				e.preventDefault();
				var thisHref = $(this).attr('href');
				$(thisHref).addClass('active').siblings().removeClass('active');
			});
		}
	}

	//slide
	if($('.slide_area').length){
		$('.slide_area .btn_slide').on('click',function(){
			$(this).parent('li').toggleClass('active');
			$(this).siblings('.cont_slide').stop().slideToggle();
		})
	}
	
	//paging
	$('.paging_area .obj').on('click',function(){
		$(this).addClass('active').siblings('.obj').removeClass('active');
	});
	
	//.btn_top 
	$(window).scroll(function() { //.btn_top 
		if ($(this).scrollTop() > 100) {
			$('.btn_top').fadeIn();
		} else {
			$('.btn_top').fadeOut();
		}
	});
	$(window).resize(function(){//.btn_top 모바일 대응
		if($('html').hasClass('mob')){
			$('.btn_top').removeAttr('style');
		}
	})
	$(".pc .btn_top").click(function() {
		$('html, body').animate({
			scrollTop : 0
		}, 400);
	});
	$(".btn_top").click(function() {
		$('html, body').animate({
			scrollTop : 0
		}, 200);
	});
	
	//레이어 팝업
	var $btn_pop = $('.btn-popup'),
		$btn_pop_close = $('.btn-pop-close, .btn-pop-confirm'),
		popLayer_wrap = '.popLayer-wrap';

	$btn_pop .on('click',function(e){
		e.preventDefault();
		gnbClose();
		var thisHref = $(this).attr("href");
		$(thisHref).removeClass('hide') .parents(popLayer_wrap) .fadeIn('fast');
		$('html').addClass('lock');
	});

	function popClose(){ // popup 닫기
		$(popLayer_wrap) .hide() .find('.section') .addClass('hide');
		$('html').removeClass('lock');
	}

	$btn_pop_close .on('click',function(){// popup 닫기 버튼 클릭
		popClose();
	})
	$(document).keyup(function(e) {// ESC 눌러 닫기
		if (e.keyCode == 27) {
			popClose();
		}
	});
	
});//end  common


$(function(){
	resizeHandler();//gnb
	pageTitle();
	lnb();
	quickMenu();
	// mainVisual();
});//end figure

function mainVisual() {
	var $mainVisual = $('.pc.main #wrap');

	// if( $(window).scrollTop() > 250 ){
	// 	$mainVisual.addClass('viewContent');
	// }
	console.log($(window).scrollTop())

	$(window).scroll(function(){
		var scrollOffset = $(this).scrollTop();
		console.log(scrollOffset);

		if( scrollOffset > 0 ){
			$mainVisual.addClass('viewContent');
		} else if( scrollOffset <= 0 ) {
			$mainVisual.removeClass('viewContent');
		}
	});
};

function resizeHandler() {
	if( $('html').hasClass('pc') ) {
		removeEvent();
		pcNav();
	}else if( $('html').hasClass('mob') ){
		removeEvent();
		mobileNav();
	}
};
function removeEvent() {
	$('.gnb-container *').off('mouseover mouseout mouseleave focus blur click');
	$('.gnb .sub-menu').removeClass('active');
};
function pcNav(){
	var $1depth_item = $('.gnb .menu > .menu-item'),
		$gnb = $('.gnb'),
		$2depth_wrap = $('.gnb .sub-menu');

	$1depth_item.mouseover(function(){
		$2depth_wrap.removeClass('active');
		$(this).find('.sub-menu').addClass('active');
	});
	$gnb.mouseleave(function(){
		$2depth_wrap.removeClass('active');
	});
};
function mobileNav(){
	var	$1depth_item = $('.mob .gnb .menu > .menu-item'),
		$1depth_itemLink = $('.mob .gnb .menu > .menu-item > a'),
		$btn_menu = $('.mob .btn-menu'),
		$gnb = $('.mob .gnb');

	$btn_menu .on('click', function(){
		$gnb .addClass('open');
		$('html').addClass('lock');
		$gnb.find('.gnb-wrap').append("<div class='gnbwrap-after'></div>");
		$('.gnbwrap-after').on('click',function(){
			gnbClose();
		});
	});

	$1depth_item.append('<i class="icon ico-plus-thin"></i>');

	$1depth_item.click(function(){
		$(this).find('.sub-menu').toggleClass('active');
		$(this).find('.icon').toggleClass('ico-minus-thin').toggleClass('ico-plus-thin');

		// gnb 스크롤 관련 : 미구현
		// var listH = $('.mob .gnb-container').height(),
		// 	contH = $('.mob .menu-gnb-container').height();
		// if ( listH < contH ){}
	});

	$1depth_itemLink.click(function(e){
		e.preventDefault();
	});

	$('.mob .gnb .btn-menu-close').on('click', function(){
		gnbClose();
	});
};
function gnbClose (){
	$('#gnb').removeClass('open');
	$('html').removeClass('lock');
	$('#gnb').find('.gnbwrap-after').remove();
};
//end : GNB

function quickMenu(){
	var $btn_open = $('.quick-menu.pc .link-item.talk'),
		$btn_close = $('.quick-menu.pc button.btn-close'),
		$quickMenuPc = $('.quick-menu.pc'),
		$quickMenuMob = $('.popLayer-wrap'),
		quickMenuCont = '.quickMenu-content';

	$btn_open.on('click', function(){
		$quickMenuPc .addClass('open');
	});
	$btn_close.on('click', function(){
		$quickMenuPc .removeClass('open');
	});
	$(document).keyup(function(e) {// ESC 눌러 닫기
		if (e.keyCode == 27) {
			$quickMenuPc .removeClass('open');
		}
	});

	if( $('html').hasClass('pc') ){
		$quickMenuMob .find(quickMenuCont).remove()
	}else if( $('html').hasClass('mob') ){
		$quickMenuPc .find(quickMenuCont).remove()
	}
};
function pageTitle(){
	var currentMenu = $('#menu-gnb > [class*="current_"] > a'),
		menuTxt = currentMenu.text(),
		pageTitle = $('.page-title .tit');
	pageTitle .text(menuTxt).css('opacity','1');
		
	var currentIndex = $('#menu-gnb > [class*="current_"]').index(),
		$visual = $('.header .visual');
	$visual .addClass('bg'+currentIndex);
};
function lnb(){
	var currentMenu = $('#menu-gnb > [class*="current_"]');

	if ( currentMenu .hasClass('menu-item-has-children') ){
		var listWrap = currentMenu.find('.sub-menu'),
			target = $('.lnb-container');

		listWrap.clone().appendTo(target);
		target.find('li').removeAttr('id');
	}

	var $lnb = $('nav.lnb'),
		$menuItem = $('nav.lnb .menu-item');

	if( !$menuItem.hasClass('current-menu-item') ) {
		$menuItem.first().children('a').css({
			'background-color' : '#6e4160',
			'color' : '#fff'
		});
	}
};

$(function(){
	$('#click').click();//태스트용 클릭 이벤트
});