(function(){ // 외부 라이브러리와 충돌을 막고자 모듈화.
// 브라우저 및 버전을 구하기 위한 변수들.
	'use strict';
	var agent = navigator.userAgent.toLowerCase(),
		name = navigator.appName,
		browser;

	// MS 계열 브라우저를 구분하기 위함.
	if(name === 'Microsoft Internet Explorer' || agent.indexOf('trident') > -1 || agent.indexOf('edge/') > -1) {
		browser = 'ie';
	} 
    else if(agent.indexOf('safari') > -1) { // Chrome or Safari
		if(agent.indexOf('opr') > -1) { // Opera
			browser = 'opera';
		} else if(agent.indexOf('chrome') > -1) { // Chrome
			browser = 'chrome';
		} else { // Safari
			browser = 'safari';
		}
	} else if(agent.indexOf('firefox') > -1) { // Firefox
		browser = 'firefox';
	}

	// IE: ie7~ie11, Edge, Chrome: chrome, Firefox: firefox, Safari: safari, Opera: opera
	document.getElementsByTagName('html')[0].className = browser;
}());
$(function () {//디바이스 체크
	var filter = 'win16|win32|win64|mac|macintel|linux i686';
	if(navigator.platform){
		if(filter.indexOf(navigator.platform.toLowerCase()) < 0){
			$('html').addClass('mob');
		}else{
			$('html').addClass('pc');
		}
	}
	if($('html').hasClass('mob')){
		if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
			$('html').addClass('ios');
		}else{
			$('html').addClass('android');
		}
	}
});