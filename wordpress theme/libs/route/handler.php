<?php
    class _CustomRouteHandler {
        public function __construct() {
            add_filter('query_vars', array($this, 'add_query_vars'));
            add_action('parse_request', array($this, 'sniff_requests'), 0);
            add_action('init', array($this, 'add_endpoint'), 0);
            add_action('wp_loaded', array($this, 'my_flush_rules'));
        }

        public function my_flush_rules(){
            $rules = get_option( 'rewrite_rules' );
            if ( ! isset( $rules['^v1/([^/]+)/?'] ) ) {
                global $wp_rewrite;
                $wp_rewrite->flush_rules(false);
            }
        }

        public function add_query_vars($qvars) {
            $qvars[] = '__service';
            return $qvars;
        }

        public function sniff_requests() {
            global $wp;
            global $wpdb;

            if ( !isset($wp->query_vars['__service']) && $wp->query_vars['__service'] !== 'api' ) {
                return;
            }

            $response = array();
            $_params;

            if ( !in_array($_SERVER['REQUEST_METHOD'], array('GET', 'POST', 'OPTIONS')) ) {
                header("HTTP/1.0 404 Not Found");
                exit;
            }

            if ( $_SERVER['REQUEST_METHOD'] === 'OPTIONS' ) {
                exit;
            }

            if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
                $entityBody = file_get_contents('php://input');
                $entityBody = preg_replace('/^data=/', '', $entityBody);
                $_params = $this->jsonDecode($entityBody);
            }

            $_hasKeys = array('data');
            if ( $_SERVER['REQUEST_METHOD'] === 'GET' ) {
                $this->has_key($_GET, $_hasKeys);

                $_params = $this->jsonDecode($_GET['data']);
            }

            # check if has key from params
            $_hasKeys[] = 'code';
            $this->has_key($_params, $_hasKeys);

            # call action
            $_methodName = 'post_' . $_params->code;
            $this -> $_methodName($wpdb, $_params->data);

            # end handler
            exit;
        }

        private function out_json(&$data) {
            $_out = $this->jsonEncode($data);
            echo $_out;
            echo "\n";
            exit;
        }

        public function add_endpoint() {
            add_rewrite_rule('^v1/([^/]+)/?','index.php?__service=$matches[1]','top');
            flush_rewrite_rules();
        }

        private function dbErr(&$db) {
            if ($db -> last_error !== '') {
                var_dump($wpdb->last_result);
                exit;
            }
        }

        private function getUserInfo() {
            $user = wp_get_current_user();
            return $user -> ID;
        }

        private function post_new($db, &$params) {
            $this->check_permission();

            # check if has key from params
            $_requiredKeys = array('category', 'title', 'content', 'post_type');
            $this->has_key($params, $_requiredKeys);

            $params -> uid = $this -> getUserInfo();
            $params -> tid = $db -> get_var (
                $db -> prepare(
                    "SELECT term_taxonomy_id FROM wp_term_taxonomy tx INNER JOIN wp_terms t ON tx.term_id = t.term_id WHERE t.slug = %s",  $params->category
                )
            );

            $this -> dbErr($db);

            $_requiredKeys = array('uid', 'tid');
            $this->has_data($params, $_requiredKeys);

            // insert wp_posts
            $db -> query (
                $db -> prepare(
                    "
                        INSERT INTO wp_posts
                        (
                            post_author, post_date, post_date_gmt
                            ,post_content, post_title, post_type
                        )
                        VALUES
                        (
                            # post author, post_date, post_date_gmt
                              %d         , NOW()    , NOW()
                            # post_content, post_title, post_type
                            , %s          , %s        , %s
                        )
                    ", $params -> uid, $params -> content, $params -> title, $params -> post_type
                )
            );

            $this -> dbErr($db);

            $params -> pid = $db -> get_var("SELECT ID FROM wp_posts ORDER BY ID DESC LIMIT 1");

            // insert term relationships
            $db -> query(
                $db -> prepare(
                    "
                        INSERT INTO wp_term_relationships
                        (object_id, term_taxonomy_id)
                        VALUES
                        (%d, %d)
                    ", $params -> pid, $params -> tid
                )
            );

            $this -> dbErr($db);

            $result['result'] = 'success';
            $result['status'] = 'success';
            $this -> out_json($result);
        }

        private function post_update($db, &$params) {
            $this->check_permission();

            # check if has key from params
            $_requiredKeys = array('pid');
            $this->has_key($params, $_requiredKeys);

            $_pid = $db -> query( $db -> prepare ("SELECT ID FROM wp_posts WHERE ID=%d", $params -> pid) );
            $this -> dbErr($db);
            if ( empty($_pid) ) {
                echo json_encode( array('status'=> 'error', 'message'=> 'empty data'));
                exit;
            }

            $update = array();
            $update[] = 'post_modified = NOW()';
            $update[] = 'post_modified = NOW()';
            if (array_key_exists('title', $params)) {
                $update[] = $db -> prepare("post_title = %s", $params -> title);
            }

            if (array_key_exists('content', $params)) {
                $update[] = $db -> prepare("post_content = %s", $params -> content);
            }

            $updateQuery = implode($update, ',');
            $db -> query (
                $db -> prepare (
                    "UPDATE wp_posts set $updateQuery WHERE ID=%d", $params->pid
                )
            );

            $this -> dbErr($db);

            if (array_key_exists('category', $params)) {
                $params -> totid = $db -> get_var (
                    $db -> prepare(
                        "SELECT term_taxonomy_id FROM wp_term_taxonomy tx INNER JOIN wp_terms t ON tx.term_id = t.term_id WHERE t.slug = %s",  $params->category
                    )
                );

                if (!is_null($params -> totid)) {
                    $db -> query (
                        $db -> prepare (
                            "UPDATE wp_term_relationships set term_taxonomy_id=%d WHERE object_id=%d AND term_taxonomy_id=%d", $params->totid, $params->pid, $params->tid
                        )
                    );
                }
            }

            $this -> dbErr($db);

            $result['result'] = 'success';
            $result['status'] = 'success';
            $this -> out_json($result);
        }

        private function post_remove($db, &$params) {
            $this->check_permission();

            # check if has key from params
            $_requiredKeys = array('tid', 'pid');
            $this->has_key($params, $_requiredKeys);

            $_pid = $db -> query( $db -> prepare ("SELECT ID FROM wp_posts WHERE ID=%d", $params -> pid) );
            if ( is_null($_pid) ) {
                echo json_encode( array('status'=> 'error', 'message'=> 'empty data'));
                exit;
            }

            $db -> query (
                $db -> prepare (
                    "DELETE FROM wp_posts WHERE ID=%d", intval($params->pid)
                )
            );

            $this -> dbErr($db);

            $db -> query (
                $db -> prepare (
                    "DELETE FROM wp_term_relationships WHERE object_id=%d AND term_taxonomy_id=%d", intval($params->pid), intval($params->tid)
                )
            );

            $this -> dbErr($db);

            $result['result'] = 'success';
            $result['status'] = 'success';
            $this -> out_json($result);
        }

        private function post_get($db, &$params) {
            $_requiredKeys = array('pid');
            $this->has_key($params, $_requiredKeys);

            $post = $db -> get_row(
                $db -> prepare(
                    "
                        SELECT
                            p.ID as pid
                            ,p.post_title
                            ,p.post_content
                            #,p.post_date
                            ,DATE_FORMAT(post_date, '%e %M, %Y') as post_date
                            ,t.name as term_name
                            ,t.slug as term_slug
                            ,tt.term_taxonomy_id as tid
                        FROM
                            wp_posts p
                        INNER JOIN
                            wp_term_relationships tr
                        ON
                            tr.object_id = p.ID
                        INNER JOIN
                            wp_term_taxonomy tt
                        ON
                            tr.term_taxonomy_id = tt.term_taxonomy_id
                        INNER JOIN
                            wp_terms t
                        ON
                            tt.term_id = t.term_id
                        WHERE
                            1=1
                        AND
                            p.post_type = 'event_news'
                        AND
                            p.ID = %d
                    ", $params -> pid
                )
            );

            $r = array(
                'a' => $db -> prepare(
                    "
                        SELECT
                            p.ID as pid
                            ,p.post_title
                            ,p.post_content
                            ,p.post_date
                            ,t.name as term_name
                            ,t.slug as term_slug
                            ,tt.term_taxonomy_id as tid
                        FROM
                            wp_posts p
                        INNER JOIN
                            wp_term_relationships tr
                        ON
                            tr.object_id = p.ID
                        INNER JOIN
                            wp_term_taxonomy tt
                        ON
                            tr.term_taxonomy_id = tt.term_taxonomy_id
                        INNER JOIN
                            wp_terms t
                        ON
                            tt.term_id = t.term_id
                        WHERE
                            1=1
                        AND
                            p.post_type = 'event_news'
                        AND
                            p.ID = %d
                    ", $params -> pid
                )
            );

            $this -> dbErr($db);
            $this -> out_json($post);
        }

        private function post_lists($db, &$params) {
            $this->has_key($params, $_requiredKeys);
            $offset = 0;
            $limit = 0;
            $page = 0;

            // column
            $selColumn = array();
            $selColumn[] = 'p.ID as pid';
            $selColumn[] = 'p.post_title';
            $selColumn[] = 'p.post_content';
            $selColumn[] = 'p.post_date';
            $selColumn[] = 't.name as term_name';
            $selColumn[] = 't.slug as term_slug';

            $cntColumn = ' COUNT(*) as cnt ';

            // where
            $where = array();
            $where[] = '1=1';
            $where[] = "p.post_type='event_news'";
            if ( array_key_exists('title', $params) ) {
                $where[] = $db -> prepare('p.post_title LIKE %s', '%' . $params -> title . '%');
            }
            if ( array_key_exists('content', $params) ) {
                $where[] = $db -> prepare('p.post_content LIKE %s', '%'. $params -> content . '%');
            }
            if ( array_key_exists('category', $params) ) {
                $where[] = $db -> prepare('t.slug = %s', $params -> category);
            }

            // order
            $order = " ORDER BY p.ID DESC ";

            // limit
            if ( array_key_exists('limit', $params) ) {
                $limit = intval($params -> limit);
            }

            if ( array_key_exists('page', $params) ) {
                $page = intval($params -> page);
                if ($page < 1) {
                    $page = 1;
                }

                $offset = ($page - 1) * $limit;
            }

            $limit = " LIMIT $offset, $limit";

            // query body
            $queryBody = 'SELECT %s FROM wp_posts p INNER JOIN wp_term_relationships tr ON tr.object_id = p.ID INNER JOIN wp_term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id INNER JOIN wp_terms t ON tt.term_id = t.term_id WHERE %s %s %s';

            $_selColumn = implode($selColumn, ', ');
            $_where = implode($where, ' AND ');
            $list = $db -> get_results(sprintf($queryBody, $_selColumn, $_where, $order, $limit));
            $this -> dbErr($db);

            $count = $db -> get_var(sprintf($queryBody, $cntColumn, $_where, "", ""));
            $this -> dbErr($db);

            $result['data'] = $list;
            $result['count'] = intval($count);
            $result['page'] = $page;
            $result['limit'] = intval($params -> limit);

            $this -> out_json($result);
        }

        private function has_key(&$target, &$searchItems) {
            $isObj = false;
            if (gettype($target) === 'object') {
                $isObj = true;
                $target = (array) $target;
            }

            if (!is_null($searchItems)) {
                foreach ($searchItems as $s) {
                    if ( !array_key_exists($s, $target) ) {
                        echo json_encode( array('status'=> 'error', 'message'=> 'empty data'));
                        exit;
                    }
                }
            }

            if ($isObj) {
                $target = (object) $target;
            }

            return true;
        }

        private function has_data(&$target, &$searchItems) {
            $isObj = false;
            if (gettype($target) === 'object') {
                $isObj = true;
                $target = (array) $target;
            }

            foreach ($searchItems as $s) {
                if ( !array_key_exists($s, $target) ) {
                    echo json_encode( array('status'=> 'error', 'message'=> 'empty data'));
                    exit;
                }

                if ( empty($target[$s]) ) {
                    echo json_encode( array('status'=> 'error', 'message'=> 'empty data'));
                    exit;
                }

                if ( is_null($target[$s]) ) {
                    echo json_encode( array('status'=> 'error', 'message'=> 'empty data'));
                    exit;
                }
            }

            if ($isObj) {
                $target = (object) $target;
            }

            return true;
        }

        private function jsonEncode(&$data) {
            $json = json_encode($data);
            if ($json === null && json_last_error() !== JSON_ERROR_NONE) {
                echo json_encode( array('status'=> 'error', 'message'=> 'json data is incorrect'));
                exit;
            }

            return $json;
        }

        private function jsonDecode(&$data) {
            $json = json_decode($data);

            if ($json === null) {
                $json = json_decode(stripslashes($data));
            }

            if ($json === null && json_last_error() !== JSON_ERROR_NONE) {
                echo json_encode( array('status'=> 'error', 'message'=> 'json data is incorrect'));
                exit;
            }

            return $json;
        }

        private function check_permission() {
            if (!$this->role_filter()) {
                echo json_encode( array('status'=> 'error', 'message'=> 'permission denied'));
                exit;
            }
        }

        private function role_filter() {
            if (!is_user_logged_in()) {
                return false;
            }

            $user = wp_get_current_user();
            foreach ( $user->roles as $role ) {
                if ( in_array( $role, array('administrator', 'editor') ) ) {
                    return true;
                }
            }
            return false;
        }

    }

    new _CustomRouteHandler();
?>
