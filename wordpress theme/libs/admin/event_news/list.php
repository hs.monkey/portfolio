<style>
    td.emptyLists {
        text-align: center;
    }

    #pagination { text-align:center; margin:30px auto 0; }
    #pagination span { font-size:9pt; }
    #pagination .prev, #pagination .next, #pagination .first, #pagination .end { width:23px; height:23px; display:inline-block; line-height:999; overflow:hidden; border:1px solid #E5E7E9; }
    #pagination .page span, #pagination .page a { width:23px; height:18px; display:inline-block; overflow:hidden; border:1px solid #E5E7E9; padding:5px 0 0 0; }
    #pagination .first { background:url(/images/content/page_first.png) no-repeat center; margin:0; }
    #pagination .prev { background:url(/images/content/page_prev.png) no-repeat center; margin:0 10px 0 0; }
    #pagination .next { background:url(/images/content/page_next.png) no-repeat center; margin:0 0 0 10px; }
    #pagination .end { background:url(/images/content/page_last.png) no-repeat center; margin:0; }
    #pagination .page {  }
    #pagination .current { background:#6A6A6A; color:#FFF; border:1px solid #6A6A6A !important; }
    #pagination a { width:100%; height:100%; display:inline-block; font-size:9pt; }

</style>
<div class="wrap">
    <h1 class="wp-heading-inline">Event&News</h1>
    <a href="/wp-admin/admin.php?page=event_news&_page=detail" class="page-title-action">Add New</a>

    <hr class="wp-header-end">
    <!-- // hr.wp-header-end -->
    <h2 class="screen-reader-text">Filter posts list</h2>

    <form id="posts-filter">
        <div class="tablenav top">

            <div class="alignleft actions bulkactions searchOpt">
                <label for="bulk-action-selector-top" class="screen-reader-text">Select bulk action</label>
                <select name="category">
                    <option value="-1">전체</option>
                    <option value="program_event">Program & event</option>
                    <option value="notice">notice</option>
                    <option value="press_release">press_release</option>
                </select>
                <select name="searchOpt">
                    <option value="-1">전체</option>
                    <option value="title">제목</option>
                    <option value="content">내용</option>
                </select>
                <input type="search" id="post-search-input" name="searchTxt" value="">
                <input type="submit" name="filter_action" id="post-query-submit" class="button" value="Filter" onclick="_cf.search(this, event)">
            </div>
            <!-- // .tablenav -->
            <br class="clear">
        </div>
        <!-- // .tablenav  -->
        <table class="wp-list-table widefat fixed striped posts">
            <thead>
                <tr>
                    <td id="cb" class="manage-column column-cb check-column"><label class="screen-reader-text" for="cb-select-all-1">Select All</label><input id="cb-select-all-1" type="checkbox"></td>
                    <th scope="col" class="manage-column column-author column-title">Title</th>
                    <th scope="col" class="manage-column column-author column-categories">Category</th>
                    <th scope="col" class="manage-column column-author column-date">Date</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="4" class="emptyLists"> empty lists </td>
                </tr>
            </tbody>
        </table>
        <!-- // table.wp-list-table -->

        <div id="pagination"></div>
    </form>
    <!-- // form#posts-filter -->
</div>

<script src="/js/_custom_lib.js" ></script>
<script>
    var _cf = {
        post_type: 'event_news'
        ,post_body: document.getElementById('posts-filter')
        ,paging: {page: 0, limit: 10}
        ,searchOpt: null
        ,init: function() {
            this.load(0);
        }
        ,href: function(url) {
            window.location.href = url;
        }
        ,load: function(page) {
            var self = this;

            if (typeof page !== 'number') {
                return;
            }

            self.paging['page'] = page;

            var _data = {'code': 'lists'};
            if (typeof self.searchOpt === 'undifined' || self.searchOpt === null) {
                _data['data'] = {}
            } else {
                _data['data'] = self.searchOpt;
            }
            _data['data']['page'] = page;
            _data['data']['limit'] = 10;

            jQuery.ajax({
                url: '/v1/api'
                ,method: 'GET'
                ,data: 'data=' + JSON.stringify(_data)
                ,success: function(res) {
                    if (res.count < 1) {
                        return;
                    }

                    res = JSON.parse(res);
                    self.printList(self, res);
                    var pagination = _common.pagination(res.page, res.limit, res.count);
                    self.printPagination(pagination);
                }
            });
        }
        ,movePage: function(ev, page) {
            _common.cancelPrevent(ev);
            var self = this;
            self.load(page);
        }
        ,printPagination: function(data) {
            var pDiv = document.getElementById('pagination');
            _common.removeChilds(pDiv);

            var span, a, opt;

            opt = {'class': 'prev'};
            span = _common.newEl('span', opt);

            opt = null;
            a = _common.newEl('a', opt);
            a.setAttribute('onclick', '_cf.movePage(event, {0})'.format(data.prev));
            a.innerHTML = '&laquo;';
            a.style.cursor = 'pointer';

            span.appendChild(a);
            pDiv.appendChild(span);

            opt = {'class': 'page'};
            span = _common.newEl('span', opt);

            for (var i=data.startPage; i <= data.endPage; i++) {
                if (i===data.curPage) {
                    a = _common.newEl('span');
                    a.innerText = i;
                } else {
                    a = _common.newEl('a');
                    a.setAttribute('onclick', '_cf.movePage(event, {0})'.format(i));
                    a.innerText = i;
                    a.style.cursor = 'pointer';
                }
                span.appendChild(a);
            }

            pDiv.appendChild(span);

            opt = {'class': 'next'};
            span = _common.newEl('span', opt);

            a = _common.newEl('a');
            a.innerHTML = '&raquo;';
            a.style.cursor = 'pointer';
            a.setAttribute('onclick', '_cf.movePage(event, {0})'.format(data.next));
            a.setAttribute('title', '다음');
            span.appendChild(a);
            pDiv.appendChild(span);

        }
        ,search: function(el, ev) {
            _common.cancelPrevent(ev);
            var self = this;

            var _s = {};
            [].forEach.call(self.post_body.querySelector('div.searchOpt').querySelectorAll('select,input'), function(_el) {
                if ( _el.tagName.toLowerCase() === 'select') {
                    if ( _el.value === '-1' ) {
                        _s['searchOpt'] = 'title,content';
                        return;
                    }

                    if ( _el.name === 'category' ) {
                        _s['category'] = _el.value;
                    }

                    if ( _el.name === 'searchOpt' ) {
                        _s['searchOpt'] = _el.value;
                    }
                } else if ( _el.getAttribute('type') === 'search' ) {
                    _s['searchTxt'] = _el.value;
                }
            });

            var _r = {};
            var opts = _s['searchOpt'].split(',');
            if (_s['searchTxt'].length > 0) {
                for (var i=0, e=opts.length; i<e; i++) {
                    _r[opts[i]] = _s['searchTxt'];
                }
            }

            if (_s.hasOwnProperty('category')) {
                _r['category'] = _s['category'];
            }


            self.searchOpt = _r;
            self.load(0);
        }
        ,printList: function(self, res) {
            var tbody, tr, th, td, cb, opt;
            tbody = self.post_body.querySelector('table tbody');
            _common.removeChilds(tbody);

            [].forEach.call( res.data, function(row, idx) {
                tr = _common.newEl('tr', null);

                th = _common.newEl('th', null);
                opt = {};
                opt['type'] = 'checkbox';
                opt['data-pid'] = row.pid;
                opt['name'] = 'select-post-items';
                cb = _common.newEl('input', opt);
                th.appendChild(cb);
                tr.appendChild(th);

                opt = {};
                opt['onclick'] = "_cf.href('/wp-admin/admin.php?page=event_news&_page=detail&pid={0}')".format(row.pid);

                td = _common.newEl('th', opt);
                td.innerText = row.post_title;
                td.style.cursor = 'pointer';
                tr.appendChild(td);

                td = _common.newEl('th', opt);
                td.innerText = row.term_name;
                td.style.cursor = 'pointer';
                tr.appendChild(td);

                td = _common.newEl('th', null);
                td.innerText = row.post_date;
                tr.appendChild(td);

                tbody.appendChild(tr);
            });
        }
    }

    _cf.init();
</script>
