<div class="wrap">
    <h1 class="wp-heading-inline">Add New Event&News</h1>
    <hr class="wp-header-end">

    <form name="post" method="post">
        <input type="hidden" name="post_id" value="0" />
        <div id="poststuff">
            <div id="post-body" class="metabox-holder columns-2">
                <div id="post-body-content" style="position: relative;">
                    <div id="titlediv">
                        <div id="titlewrap">
                            <label class="" id="title-prompt-text" for="title">Enter title here</label>
                            <input type="text" name="post_title" size="30" value="" onfocusin="_cf.focusIn(this)" onfocusout="_cf.focusOut(this)" id="title" spellcheck="true" autocomplete="off">
                        </div>
                        <!-- // div#titlewrap -->
                    </div>
                    <!-- // div#titlediv -->

                    <div id="postdivrich" class="postarea wp-editor-expand">
                        <?php wp_editor( '', 1 ,array('textarea_name' => 'post_content')); ?>
                    </div>
                    <!-- // div#postdivrich -->
                </div>
                <!-- // div#post-body-content -->

                <div id="postbox-container-1" class="postbox-container">
                    <div id="side-sortables" class="meta-box-sortables ui-sortable">
                        <div id="submitdiv" class="postbox">
                            <button type="button" class="handlediv" aria-expanded="true">
                                <span class="screen-reader-text">Toggle panel: Publish</span>
                                <span class="toggle-indicator" aria-hidden="true"></span>
                            </button>
                            <!-- // button.handlediv -->
                            <h2 class="hndle ui-sortable-handle"><span>Category & Publish</span></h2>

                            <div class="inside">
                                <div class="submitbox" id="submitpost">
                                    <div id="minor-publishing">
                                        <div id="minor-publishing-actions" style="text-align: left;">
                                            <div id="save-action">
                                            <ul>
                                                <li>
                                                    <input type='radio' name='post_category' value='program_event' /> Program & Event
                                                </li>
                                                <li>
                                                    <input type='radio' name='post_category' value='notice' /> Notice
                                                </li>
                                                <li>
                                                    <input type='radio' name='post_category' value='press_release' /> Press Release
                                                </li>
                                            </ul>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <!-- -->
                                    <div id="major-publishing-actions">
                                        <div id="delete-action">
                                            <a class="submitdelete deletion" href="#" onclick="_cf.remove(this, event)">Delete</a>
                                        </div>
                                        <!-- // div#delete-action -->
                                        <div id="publishing-action">
                                            <input type="submit" name="publish" id="publish" class="button button-primary button-large" value="Save" onclick="_cf.save(this, event)" >
                                        </div>
                                        <!-- // div#publishing-action -->
                                        <div class="clear"></div>
                                    </div>
                                    <!-- -->
                                </div>
                                <!-- // div.submitbox -->
                            </div>
                            <!-- // div.inside -->
                            <!-- -->
                        </div>
                        <!-- // div#submitdiv -->

                    </div>
                    <!-- // div#side-sortables -->
                </div>
                <!-- // div#post-box-container-1 -->
            </div>
            <!-- // div.metabox-holder -->
        </div>
        <!-- // div.poststuff -->
    </form>
</div>

<script src="/js/_custom_lib.js" ></script>
<script>
    var _cf = {
        post_type: 'event_news'
        ,post_body: document.getElementById('post-body')
        ,params: null
        ,init: function() {
            this.params = _common.getParams();
            if (!this.params.hasOwnProperty('pid')) {
                this.post_body.querySelector('div#delete-action').style.display = 'none';
            } else {
                this.getItem();
            }
        }
        ,getItem: function() {
            var self = this;
            var _data = {'code': 'get', 'data': {}};
            _data['data']['pid'] = self.params['pid'];

            jQuery.ajax({
                url: '/v1/api'
                ,method: 'GET'
                ,data: 'data=' + JSON.stringify(_data)
                ,success: function(res) {
                    if (typeof res === 'undefined' || res === null) {
                        alert('잘못된 접근입니다.');
                        location.href = '/wp-admin/admin.php?page=event_news';
                        return;
                    }
                    res = JSON.parse(res);
                    document.getElementById('title-prompt-text').className += ' screen-reader-text';
                    self.post_body.querySelector('input[name=post_title]').value = res.post_title;
                    self.post_body.querySelector('textarea[name=post_content]').value = res.post_content;
                    self.post_body.querySelector('input[type=radio][name=post_category][value=' + res.term_slug + ']').checked = true;
                    self.params['tid'] = res.tid;
                    self.params['pid'] = res.pid;
                }
            });
        }
        ,save: function(el, ev) {
            _common.cancelPrevent(ev);
            var self = this;

            var _data = {};
            _data['code'] = 'new';
            _data['data'] = {'post_type': self.post_type }

            if (this.params.hasOwnProperty('pid')) {
                _data['data']['pid'] = this.params['pid'];
                _data['data']['tid'] = this.params['tid'];
                _data['code'] = 'update';
            }

            var getValItems = ['input[name=post_title]' ,'textarea[name=post_content]' ,'input[type=radio][name=post_category]:checked'];
            var _krNm = ['제목' ,'내용' ,'카테고리'];
            var _val;
            for( var i=0, e=getValItems.length, el=null; i<e; i++ ) {
                el = self.post_body.querySelector(getValItems[i]);

                if (el === null) {
                    alert(_krNm[i] + '를 선택해주세요');
                    return;
                }

                _val = el.value;
                if (el.tagName.toLowerCase() === 'textarea' && /tmce-active/g.test(document.getElementById('wp-1-wrap').className)) {
                    console.log('sadfds');
                    console.log(tinyMCE.activeEditor.getContent());
                    _val = tinyMCE.activeEditor.getContent();
                }

                if (typeof _val === 'undefined' || _val.length < 1) {
                    alert(_krNm[i] + '를 입력해주세요');
                    el.focus();
                    return;
                }

                _data['data'][el.name.replace(/^post_/g, '')] = _val;
            }

            jQuery.ajax({
                url: '/v1/api'
                ,method: 'POST'
                ,data: JSON.stringify(_data)
                ,success: function(res) {
                    res = JSON.parse(res);
                    if ( res.status === 'success' ) {
                        alert('success');
                        window.location.href = '/wp-admin/admin.php?page=event_news';
                    }
                }
            });
        }
        ,remove: function(el, ev) {
            _common.cancelPrevent(ev);
            var self = this;

            var _data = {};
            _data['code'] = 'remove';
            _data['data'] = {};

            if ( !_common.hasKey(['tid', 'pid'], self.params) ) {
                alert('잘못된 요청입니다.');
                return;
            }

            _data['data']['pid'] = self.params['pid'];
            _data['data']['tid'] = self.params['tid'];

            jQuery.ajax({
                url: '/v1/api'
                ,method: 'POST'
                ,data: JSON.stringify(_data)
                ,success: function(res) {
                    res = JSON.parse(res);
                    if ( res.status === 'success' ) {
                        alert('완료');
                        window.location.href = '/wp-admin/admin.php?page=event_news';
                    }
                }
            });
        }
        ,focusIn: function(el) {
            document.getElementById('title-prompt-text').className += ' screen-reader-text';
        }
        ,focusOut: function(el) {
            if (el.value.length > 0) {
                return;
            }
            document.getElementById('title-prompt-text').className = document.getElementById('title-prompt-text').className.replace(/ screen-reader-text/g, '');
        }
    }

    _cf.init();
</script>
