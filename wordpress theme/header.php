<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
		<title>MJ 올 피부과 의원</title>
		<?php _tana_get_head_metas(); ?>
		<?php wp_head(); ?>
		<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.12.4.min.js"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/device_check.js"></script>
		<!-- {-CONFIG_CSS-} -->
		<link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css" media="all" rel="stylesheet">
		<link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/kboard.css" media="all" rel="stylesheet">
		<link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/tanaicon.min.css" media="all" rel="stylesheet">
		<link type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style.css" media="all" rel="stylesheet">
		<!--[if lt IE 9]>
			<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie8.css">
			<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.min.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.pseudo.js"></script>
			<script src="<?php echo get_template_directory_uri(); ?>/js/selectivizr-min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="skipnav">
			<a href="#content">본문가기</a>
		</div>
		
		<!-- s: #wrap -->
		<div id="wrap" class="wrap">
			<!-- s: .quick-menu -->
			<div class="quick-menu pc">
				<ul class="list-wrap">
					<li class="list-item"><a href="<?php echo do_shortcode('[path]') ?>/advice_online" class="link-item link-advice">
						<span class="tit"><i class="icon"></i>온라인상담</span>
					</a></li>
					<li class="list-item"><a href="#;" class="link-item link-after">
						<span class="tit"><i class="icon"></i>전후사진</span>
					</a></li>
					<li class="list-item"><a href="<?php echo do_shortcode('[path]') ?>/introduce/info" class="link-item link-location">
						<span class="tit"><i class="icon"></i>오시는 길</span>
					</a></li>
					<li class="list-item"><button type="button" class="link-item talk ajax-kakao">
						<span class="tit"><i class="ico-logo-kakaotalk icon"></i>카카오톡</span>
					</button></li>
					<li class="close-content"><button type="button" class="btn-close"><span class="txt">닫기</span><i class="ico-arrow-right-circle"></i><span class="txt2">or ESC key</span></button></li>
				</ul>
				<div class="quickMenu-content">
					<div id="quickMenuCont-kakao" class="section quickMenuCont-kakao">
						<!--  호출 : quickMenuCont-kakao.php -->
						<?php include 'template/popup/quickMenuCont-kakao.php'; ?>
					</div>
				</div>
			</div>
			<!-- e: .quick-menu -->
			<!-- s: #header -->
			<header id="header" class="header">
				<div class="container">
					<h1 class="header-title"><a href="<?php echo do_shortcode('[path_home]') ?>" class="link">MJ 올 피부과 의원</a></h1>
					<!-- s: #gnb -->
					<button type="button" class="btn-menu"><span class="blind">메뉴 보기</span></button>
					<nav id="gnb" class="gnb">
						<div class="gnb-wrap">
							<!-- 모바일용 화면 -->
							<div class="m-header">
								<div class="logo_area">
									<a href="<?php echo do_shortcode('[path_home]') ?>" class="link">
										<img src="<?php echo do_shortcode('[path]') ?>/images/content/logo_m_header.png" alt="MJ 올 피부과의원 logo">
									</a>
								</div>
								<div class="quick-menu">
									<ul class="list-wrap">
										<li class="list-item"><a href="<?php echo do_shortcode('[path]') ?>/advice_online" class="link-item link-advice">
											<span class="tit"><i class="icon"></i>온라인상담</span>
										</a></li>
										<li class="list-item"><a href="#;" class="link-item link-after-pic">
											<span class="tit"><i class="icon"></i>전후사진</span>
										</a></li>
										<li class="list-item"><a href="<?php echo do_shortcode('[path]') ?>/introduce/info" class="link-item link-location">
											<span class="tit"><i class="icon"></i>오시는 길</span>
										</a></li>
										<li class="list-item"><a href="#quickMenuCont-kakao" class="link-item talk btn-popup">
											<span class="tit"><i class="ico-logo-kakaotalk icon"></i>카카오톡</span>
										</a></li>
									</ul>
								</div>
							</div>
							<!-- // end : 모바일용 화면 -->
							<h2 class="blind">전체메뉴</h2>
							<div class="gnb-container">
								<?php wp_nav_menu(); ?>
							</div>
							<button type="button" class="btn-menu-close ico-cross-thin"><span class="blind">메뉴 닫기</span></button>
						</div>
					</nav>
					<!-- e: #gnb -->
				</div>
				
				<!-- s: .lnb -->
				<nav class="lnb">
					<div class="lnb-wrap">
						<h2 class="blind">소메뉴</h2>
						<div class="lnb-container"><!-- sub menu --></div>
					</div>
				</nav>
				<!-- e: .lnb -->
				
				<!-- s: .visual -->
				<div class="visual bg0">
					<div class="page-title">
						<h2 class="tit">page title</h2>
					</div>
					<div class="breadcrumbs">
						<?php
							if(function_exists('bcn_display'))
							{
								bcn_display();
							}
						?>
					</div>
				</div>
				<!-- e: .visual -->
			</header>
			<!-- e: #header -->