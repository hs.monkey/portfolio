<?php wp_footer(); ?>
<!-- s: .quick-menu -->
<div class="mob-quickMenuArea">
  <div class="quick-menu mob">
    <ul class="list-wrap">
      <li class="list-item"><a href="<?php echo do_shortcode('[path]') ?>/advice_online" class="link-item link-advice">
        <span class="tit"><i class="icon"></i>온라인상담</span>
      </a></li>
      <li class="list-item"><a href="#;" class="link-item link-after">
        <span class="tit"><i class="icon"></i>전후사진</span>
      </a></li>
      <li class="list-item"><a href="<?php echo do_shortcode('[path]') ?>/introduce/info" class="link-item link-location">
        <span class="tit"><i class="icon"></i>오시는 길</span>
      </a></li>
      <li class="list-item"><a href="#mob-quickMenuCont-kakao" class="link-item talk btn-popup">
        <span class="tit"><i class="ico-logo-kakaotalk icon"></i>카카오톡</span>
      </a></li>
    </ul>
  </div>
</div>
<!-- e: .quick-menu -->
<section class="bottom-common">
  <h3 class="blind">공통 하단 섹션</h3>
    <div class="wrap">

      <div class="rightside">
        <h4 class="blind">상담신청 폼</h4>
          <div class="contactForm">
            <?php echo do_shortcode('[contact-form-7 id="26" title="Contact form 1"]'); ?>
          </div>
      </div>

      <div class="leftside">
        <ul>
          <li>

            <dl>
              <dt>
                <p class="tit">찾아오시는 길</p>
                <a href="<?php echo do_shortcode('[path]') ?>/introduce/info" class="color-button">약도보기<span class="ico-arrow-right"></span></a>
              </dt>
              <dd>
                <p>당신의 건강한 피부를 생각하는 MJ올피부과로 오시는 길 안내입니다.</p>
              </dd>
            </dl>

          </li>
          <li>
            <ul>
              <li><span class="tel-title">대표전화<em class="colon">&colon;</em></span><span class="tel-text"><a href="tel:02-777-8275" class="link-tel">02-777-8275</a></span></li>
              <li>주소<em class="colon">&colon;</em>서울특별시 중구 명동8가길 27 (충무로 2가 11-1) 선샤인빌딩 5층</li>
              <li>팩스<em class="colon">&colon;</em>02-773-7582</li>
              <li>지하철 4호선 명동역 9번 출구</li>
            </ul>
          </li>
        </ul>
      </div>

    </div>
</section>


<footer id="footer" class="footer">
  <div class="wrap">
    <p class="address">서울특별시 중구 명동8가길 27 (충무로 2가 11-1) 선샤인빌딩 5층 <span class="br">전화<em class="colon">&colon;</em><a href="tel:02-777-8275" class="link-tel">02-777-8275</a> 팩스<em class="colon">&colon;</em>02-773-7582</span></p>
    <p class="copyright">Copyright 2018 MJ all clinic, All rights reserved.</p>
  </div>


</footer>
<!-- e: #footer -->
</div>
<!-- e: #wrap -->

<!-- layer popup -->
<div class="popLayer-wrap">
  <div class="popLayer-container">
    <div class="quickMenu-content">
      
      <!-- 퀵메뉴 -카카오톡 상담요청 -->
      <div id="mob-quickMenuCont-kakao" class="section hide mob-quickMenuCont-kakao">
        <?php include 'template/popup/mob-quickMenuCont-kakao.php'; ?>
      </div>
      <!-- //퀵메뉴 -카카오톡 상담요청 -->

    </div>
  </div>
  <div class="btn-area">
    <button class="btn-pop-close"><i class="ico-cross-thick">닫기</i></button>
  </div>
</div>
<!-- e: layer popup -->

<?php echo _tana_load_js(); ?>

</body>

</html>