(function(){ // 외부 라이브러리와 충돌을 막고자 모듈화.
// 브라우저 및 버전을 구하기 위한 변수들.
	'use strict';
	var agent = navigator.userAgent.toLowerCase(),
		name = navigator.appName,
		browser;

	// MS 계열 브라우저를 구분하기 위함.
	if(name === 'Microsoft Internet Explorer' || agent.indexOf('trident') > -1 || agent.indexOf('edge/') > -1) {
		browser = 'ie';
	} else if(agent.indexOf('safari') > -1) { // Chrome or Safari
		if(agent.indexOf('opr') > -1) { // Opera
			browser = 'opera';
		} else if(agent.indexOf('chrome') > -1) { // Chrome
			browser = 'chrome';
		} else { // Safari
			browser = 'safari';
		}
	} else if(agent.indexOf('firefox') > -1) { // Firefox
		browser = 'firefox';
	}

	// IE: ie7~ie11, Edge, Chrome: chrome, Firefox: firefox, Safari: safari, Opera: opera
	document.getElementsByTagName('html')[0].className = browser;
}());

$(function () {// common

	//디바이스 체크
	var filter = 'win16|win32|win64|mac|macintel|linux i686';
	if(navigator.platform){
		if(filter.indexOf(navigator.platform.toLowerCase()) < 0){
			$('html').addClass('mob');
		}else{
			$('html').addClass('pc');
		}
	}
	if($('html').hasClass('mob')){
		if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
			$('html').addClass('ios');
		}else{
			$('html').addClass('android');
		}
	}

	// position: fixed 사용시 ie 떨림 현상 방지
	if( navigator.userAgent.match(/Trident\/7\./) ){
		$('body').on("mousewheel", function(){
			event.preventDefault();

			var wheelDelta = event.wheelDelta,
				currentScrollPosition = window.pageYOffset;

			window.scrollTo(0, currentScrollPosition - wheelDelta);
		});
		$('body').keydown(function(e){
			e.preventDefault();
			var currentScrollPosition = window.pageYOffset;

			switch (e.which){
				case 38: //up
					window.scrollTo(0, currentScrollPosition - 120);
					break;
				case 40: //down
					window.scrollTo(0, currentScrollPosition + 120);
					break;
				default: return;
			}
		});
	}
	
	//텝 메뉴
	if ($('.tabs').length){
		$('.tabs .btn_tab').on('click', function (e) {
			$(this).parent('li').addClass('active').siblings().removeClass('active');
		});
		if ($('.tabContents').length){
			$('.tabs .btn_tab[href*="tabCont-"]').on('click', function (e) {
				e.preventDefault();
				var thisHref = $(this).attr('href');
				$(thisHref).addClass('active').siblings().removeClass('active');
			});
		}
	}

	//slide
	if( $('.slide_area').length ){
		$('.slide_area .btn_slide').on('click',function(){
			$(this).parent('li').toggleClass('active');
			$(this).siblings('.cont_slide').stop().slideToggle();
		})
	}
	
	//paging
	$('.paging_area .obj').on('click',function(){
		$(this).addClass('active').siblings('.obj').removeClass('active');
	});
	
	//.btn_top 
	$(window).scroll(function() { //.btn_top 
		if ($(this).scrollTop() > 500) {
			$('.btn_top').fadeIn();
		} else {
			$('.btn_top').fadeOut();
		}
	});
	$(".btn_top").click(function() {
		$('html, body').animate({
			scrollTop : 0
		}, 200);
	});
	
	//레이어 팝업
	var $btn_pop = $('.btn_popup'),
		$btn_pop_close = $('.btn_pop_close, .btn_pop_confirm');
		popLayer_wrap = '.popLayer_wrap';

	$btn_pop .on('click',function(e){
		e.preventDefault();
		var thisHref = $(this).attr("href");
		$(thisHref) .parents(popLayer_wrap) .fadeIn('fast');
		$('body').addClass('fixed')
	});

	function popClose(){ // popup 닫기
		$('.popLayer_wrap') .hide();
		$('body').removeClass('fixed');
	}

	$btn_pop_close .on('click',function(){// popup 닫기 버튼 클릭
		popClose();
	})
	$(document).keyup(function(e) {// ESC 눌러 닫기
		if (e.keyCode == 27) {
			popClose();
		}
	});
	
});//end  common


$(function(){
	//lnb
	if( $('.accordion').length ){ 
		var $wrap = $('.accordion'),
			title = '.accordion [class*="title_"]',
			$btn_link = $('.accordion .title_1 .btn-ajax'),
			$btn_subMenu = $('.accordion .btn-moveToId'),
			slideCont = '.accordion .slideCont',
			listWrap = '[class*="list_wrap-depth_"]';

		$(title).each(function(){ // 슬라이드 버튼 생성
			if ( $(this).next(listWrap).length ){
				$(this).append('<i class="slideCont fas fa-caret-down">하위메뉴열기</i>');
			}
		})

		$btn_link .click(function(){ // 현재 페이지 활성화 콘트롤
			if( $(this).parent('.title_1').hasClass('active') ) {
				$(this).parent('.title_1').removeClass('active');
			}else{
				$(this).parent('.title_1').addClass('currentPage active').siblings().removeClass('currentPage active');
			}
		})

		$btn_subMenu.click(function(){ //슬라이드 콘트롤
			var currentTitle = '[class*="title_"]';
			
			if( $(this).parent(currentTitle).hasClass('active') ) {
				$(this).parent(currentTitle).removeClass('active');
			}else{
				$(this).parent(currentTitle).addClass('active').siblings().removeClass('active');
				$(this).parents('li').siblings().children(currentTitle).removeClass('active');
			}
		})


		$btn_subMenu.on("click", function (e) { //페이지내 링크 이동시 효과
			e.preventDefault();
			var thisTarget = $(this).attr("href"),
				offset = $(thisTarget).offset().top-50;

			$('html, body').animate({
				scrollTop : offset
			},{
				duration:"fast",
				easing: "swing"
			});
		});

	}

})


$(function(){
	$('#click').click();
})

