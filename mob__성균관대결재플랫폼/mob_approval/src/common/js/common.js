(function(){ // 외부 라이브러리와 충돌을 막고자 모듈화.
// 브라우저 및 버전을 구하기 위한 변수들.
	'use strict';
	var agent = navigator.userAgent.toLowerCase(),
		name = navigator.appName,
		browser;

	// MS 계열 브라우저를 구분하기 위함.
	if(name === 'Microsoft Internet Explorer' || agent.indexOf('trident') > -1 || agent.indexOf('edge/') > -1) {
		browser = 'ie';
	} else if(agent.indexOf('safari') > -1) { // Chrome or Safari
		if(agent.indexOf('opr') > -1) { // Opera
			browser = 'opera';
		} else if(agent.indexOf('chrome') > -1) { // Chrome
			browser = 'chrome';
		} else { // Safari
			browser = 'safari';
		}
	} else if(agent.indexOf('firefox') > -1) { // Firefox
		browser = 'firefox';
	}

	// IE: ie7~ie11, Edge, Chrome: chrome, Firefox: firefox, Safari: safari, Opera: opera
	document.getElementsByTagName('html')[0].className = browser;
}());

$(function () {// common

	//디바이스 체크
	var filter = 'win16|win32|win64|mac|macintel|linux i686';
	if(navigator.platform){
		if(filter.indexOf(navigator.platform.toLowerCase()) < 0){
			$('html').addClass('mob');
		}else{
			$('html').addClass('pc');
		}
	}
	if($('html').hasClass('mob')){
		if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
			$('html').addClass('ios');
		}else{
			$('html').addClass('android');
		}
	}
	
	//todo 개발시 삭제 --pc화면에서 개발자 도구로 모바일 화면을 볼 때만 사용합니다.
	var fn_reWinW= function(){
		if($(window).width()>1366) {
			$('html').removeClass('mob').addClass('pc');
		}else{
			$('html').removeClass('pc').addClass('mob');
		}
	}
	fn_reWinW();
	$(window).resize(function(){
		fn_reWinW();
	});
	// ---/---
	
	//텝 메뉴
	if ($('.tabs').length){
		$('.tabs .btn_tab').on('click', function (e) {
			$(this).parent('li').addClass('active').siblings().removeClass('active');
		});
		if ($('.tabContents').length){
			$('.tabs .btn_tab[href*="tabCont-"]').on('click', function (e) {
				e.preventDefault();
				var thisHref = $(this).attr('href');
				$(thisHref).addClass('active').siblings().removeClass('active');
			});
		}
	}

	//accordion
	if($('.aco_area').length){
		$('.aco_area .btn_slide').on('click',function(){
			$(this).parent('li').toggleClass('active');
			$(this).siblings('.cont_slide').stop().slideToggle();
		})
	}
	
	//paging
	$('.paging_area .obj').on('click',function(){
		$(this).addClass('active').siblings('.obj').removeClass('active');
	});
	
	//.btn_top 
	$(window).scroll(function() { //.btn_top 
		if ($(this).scrollTop() > 100) {
			$('.btn_top').fadeIn();
		} else {
			$('.btn_top').fadeOut();
		}
	});
	$(window).resize(function(){//.btn_top 모바일 대응
		if($('html').hasClass('mob')){
			$('.btn_top').removeAttr('style');
		}
	})
	$(".pc .btn_top").click(function() {
		$('html, body').animate({
			scrollTop : 0
		}, 400);
	});
	$(".btn_top").click(function() {
		$('html, body').animate({
			scrollTop : 0
		}, 200);
	});
	
	//레이어 팝업
	var $btn_pop = $('.btn_popup'),
		$btn_pop_close = $('.btn_pop_close, .btn_pop_confirm');
		popLayer_wrap = '.popLayer_wrap';

	$btn_pop .on('click',function(e){
		e.preventDefault();
		var thisHref = $(this).attr("href");
		$(thisHref) .parents(popLayer_wrap) .fadeIn('fast');
		$('body').addClass('fixed')
	});

	function popClose(){ // popup 닫기
		$('.popLayer_wrap') .hide();
		$('body').removeClass('fixed');
	}

	$btn_pop_close .on('click',function(){// popup 닫기 버튼 클릭
		popClose();
	})
	$(document).keyup(function(e) {// ESC 눌러 닫기
		if (e.keyCode == 27) {
			popClose();
		}
	});
	
});//end  common


$(function(){
	//.bottom-btn_area 하단 여백
	if ( $('.bottom-btn_area').length ) {
		var height = $('.bottom-btn_area').height();
		$('.wrap') .css('padding-bottom',height);
	}

	// 경로 수정 팝업
	function approveMenu () {
		var $item = $('.approve_menu .btn_approve_item'),
			$btn_aco = $('.btn_aco_approve'),
			wrap = '.approve_menu_row',
			menuBar = '.approve_menu',
			txt = '';

		$item.click(function(){//결제메뉴에서 선택한 메뉴를 셀에 반영
			txt = $(this).html();

			$(this).parents(wrap).addClass('active').siblings(wrap).removeClass('active');
			$(this).parents(wrap).prev().find('.target').text(txt);
			$(wrap).removeClass('active');
			$('.btn_aco_approve').removeClass('active');
		});
		
		$btn_aco.click(function(){// 결재 메뉴 
			targetWrap = $(this).parents('tr').next();

			$('.btn_aco_approve').removeClass('active');
			if(targetWrap.hasClass('active')){
				targetWrap.removeClass('active');
			}else{
				$(wrap).removeClass('active');
				targetWrap.addClass('active');
				$(this).addClass('active');
			}
		});

		$('.sort-btn_area .rdo').on('click', function(){//정렬 버튼
			$(this).addClass('active').siblings().removeClass('active');
		});

	};approveMenu();

	function approveTree () {
		var $li = $('.approvalRoute .route li'),
			$ul = $('.approvalRoute .list_wrap ul'),
			$btn_ellip = $('.approvalRoute .btn_ellipsis'),
			$btn_folding = $('.approvalRoute .btn_folding'),
			$btn_spread = $('.approvalRoute .btn_spread')
			;
		if($ul.length){
			$ul.each(function(){
				$(this).parent('li').addClass('plus');
			})
		}
		$btn_ellip.on('click',function(){
			$li.children('.txt').toggleClass('ellip_1');
		});
		$btn_spread.on('click',function(){
			$ul.show().each(function(){
				$(this).parent('li').addClass('minus').removeClass('plus');
			});
		});
		$btn_folding.on('click',function(){
			$ul.each(function(){
				$(this).parent('li').addClass('plus').removeClass('minus');
			}).hide();
		});
		$li.click(function(e){
			e.stopPropagation();
			if($(this).hasClass('plus')){
				$(this).removeClass('plus').addClass('minus').children('ul').slideDown('fast');
			}else if($(this).hasClass('minus')){
				$(this).removeClass('minus').addClass('plus').children('ul').slideUp('fast');
			}
		});

	};approveTree();

	// 메뉴
	function menu () {
		var $btn = $('.menu_bar .btn_menu'),
			$bar = $('.menu_bar .slideMenu'),
			$menu = $('.menu_bar .menu-list_wrap');

		$btn .on('click',function(){
			$('.wrap').toggleClass('dim');
			$('body').toggleClass('fixed');
			$menu.toggleClass('hide');
			$bar.toggleClass('active');
		});
	};menu();

	// 첨부파일 리스트
	function attachFile () {
		var $btn = $('.attachFile .btn_slide'),
			$bar = $('.attachFile .cont_top'),
			$menu = $('.attachFile .files');

		$btn .on('click',function(){
			$menu.slideToggle('fast');
			$bar.toggleClass('active');
		});
	};attachFile();

	$('.popup .table_wrap .slideCont .btn_aco') .on('click', function(){
		$(this).toggleClass('active').parents('.slideCont').children('.detailedView').slideToggle();
	});

	// 검색창 검색 조건 대응
	function requirement() {
		var $wrap = $('.searchRequirement_wrap'),
			$repuirement = $('.searchRequirement_wrap .requirement select');
			
		$repuirement.change(function(){
			var repuirement = $repuirement.val(),
				$case = $('.searchRequirement_wrap [class*="case_"]'),
				$case_input = $('.searchRequirement_wrap .case_input'),
				$case_select = $('.searchRequirement_wrap .case_select');

			$case.removeClass('active')
			if( repuirement == 'opt_nextInput' ){
				$wrap.children('.case_input').addClass('active')
			} else if ( repuirement == 'opt_nextSelect' ) {
				$wrap.children('.case_select').addClass('active')
			}
		});
	};requirement();


})


$(function(){
	$('#click').click();
})

