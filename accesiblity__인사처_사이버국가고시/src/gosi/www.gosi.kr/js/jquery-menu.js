$(document).ready(function () {


	$(".naviMenu ul li").hover(function () {
		//    if($(".submenu").is(':not(":animated")')){
		//      $(".submenu").slideDown();
		//    }
		$(".submenu").show();
	});

	$(".naviLayout").mouseleave(function () {
		//    $(".submenu").slideUp();  
		$(".submenu").hide();
	});

	$(".naviMenu ul li a").click(function () {
		$(".naviMenu ul li").removeClass("on");
	});


	$(".naviMenu ul li").hover(function () {
		var index = $(".naviMenu ul li").index(this);
		$(".submenuUl li").removeClass("on");
		$(".submenuUl > li").eq(index).addClass("on");
	});

	$(".submenu ul li").hover(function () {
		$(".submenuUl li").removeClass("on");
		if ($(this).parent().parent().hasClass("submenuLi")) {
			$(this).parent().parent().addClass("on");
		} else {
			$(this).addClass("on");
		}
	});

	/* 모바일 */

	$(".top_menu_icon").click(function () {
		$(".MnaviLayout").show();
		$('body').addClass('__fixed');
	});

	$(".m_close").click(function () {
		$(".MnaviLayout").hide();
		$('body').removeClass('__fixed');
	});

	$(".m_navi a").click(function () {
		$(".MnaviLayout").hide();
	});

	$(".con_left ul li").click(function () {
		var index = $(".con_left ul li").index(this);

		$(".con_left ul li a").removeClass("on");
		$(this).find("a").addClass("on");

		$(".m_navi").css("display", "none");
		$(".m_navi_con > li").eq(index).show();
	});

});

$(function () { // [접근성] 추가 소스 
	pcGnbActive();
	mobGnbActive();
	boardTab();
});

function pcGnbActive() {
	var $linkItem = $('.g-naviMenu .link-item'),
		$gnb = $('.g-naviLayout'),
		$list_main = $('.g-naviMenu .mainMenu-list');

	mouseHover($gnb);
	mouseHover($list_main);
	focus($linkItem, $gnb);
};

function mobGnbActive() {
	var $mainMenuItem = $('.mob-gnb .mainMenu-item');

	tabCtrl($mainMenuItem, false);
};

function boardTab() {
	$('.brd-tab .tab-item').on('click', function (e) {
		e.preventDefault();
		var thisHref = $(this).attr('href'),
			focusTarget = $(thisHref).children('li').first().find('a');
		setTimeout(function () {
			focusTarget.focus();
		}, 150);
	});
};

function mouseHover(selector) {
	selector.hover(function () {
		$(this).addClass('__active');
	}, function () {
		$(this).removeClass('__active');
	});
};

function focus(selector, target) {
	selector.focus(function (e) {
		e.preventDefault();
		target.addClass('__active');
	});
	selector.blur(function (e) {
		e.preventDefault();
		target.removeClass('__active');
	});
};

function tabCtrl(tab, tabContent) {
	tab.on('click', function (e) {
		//e.preventDefault();
		$(this).parent('li').addClass('__active').siblings().removeClass('__active');
	});
	if (tabContent) {
		tab.on('click', function (e) {
			e.preventDefault();
			var thisHref = $(this).attr('href');
			$(thisHref).addClass('__active').siblings().removeClass('__active');
		});
	}
};