(function(){ // 외부 라이브러리와 충돌을 막고자 모듈화.
// 브라우저 및 버전을 구하기 위한 변수들.
	'use strict';
	var agent = navigator.userAgent.toLowerCase(),
		name = navigator.appName,
		browser;

	// MS 계열 브라우저를 구분하기 위함.
	if(name === 'Microsoft Internet Explorer' || agent.indexOf('trident') > -1 || agent.indexOf('edge/') > -1) {
		browser = 'ie';
	} else if(agent.indexOf('safari') > -1) { // Chrome or Safari
		if(agent.indexOf('opr') > -1) { // Opera
			browser = 'opera';
		} else if(agent.indexOf('chrome') > -1) { // Chrome
			browser = 'chrome';
		} else { // Safari
			browser = 'safari';
		}
	} else if(agent.indexOf('firefox') > -1) { // Firefox
		browser = 'firefox';
	}

	// IE: ie7~ie11, Edge, Chrome: chrome, Firefox: firefox, Safari: safari, Opera: opera
	document.getElementsByTagName('html')[0].className = browser;
}());

$(function () {
	//디바이스 체크
	var filter = 'win16|win32|win64|mac|macintel|linux i686';
	if(navigator.platform){
		if(filter.indexOf(navigator.platform.toLowerCase()) < 0){
			$('html').addClass('mob');
		}else{
			$('html').addClass('pc');
		}
	}
	if($('html').hasClass('mob')){
		if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
			$('html').addClass('ios');
		}else{
			$('html').addClass('android');
		}
	}
	
	//todo 개발시 삭제 --pc화면에서 개발자 도구로 모바일 화면을 볼 때만 사용합니다.
	var fn_reWinW= function(){
		if($(window).width()>1366) {
			$('html').removeClass('mob').addClass('pc');
		}else{
			$('html').removeClass('pc').addClass('mob');
		}
	}
	fn_reWinW();
	$(window).resize(function(){
		fn_reWinW();
	});
	// ---/---
	
	//텝 메뉴
	if ($('.tabs').length){
		$('.tabs .btn_tab').on('click', function (e) {
			$(this).parent('li').addClass('active').siblings().removeClass('active');
		});
		if ($('.fn_tab_content').length){
			$('.tabs .btn_tab[href*="tab_content"]').on('click', function (e) {
				e.preventDefault();
				var thisHref = $(this).attr('href');
				$(thisHref).addClass('active').siblings().removeClass('active');
			});
		}
	}
	if ($('.list_wrap_result').length){ //조건 검색결과 리스트 화면
		$('.result_box.main .list_wrap_result .btn_small_detail').on('click', function (e) {
			$(this).parents('.lst_result').addClass('active').siblings().removeClass('active');
		});
	}
	
	//accordion
	if($('.aco_area').length){
		$('.aco_area .btn_slide').on('click',function(){
			$(this).parent('li').toggleClass('active');
			$(this).siblings('.cont_slide').stop().slideToggle();
		})
	}
	
	//paging
	$('.paging_area .obj').on('click',function(){
		$(this).addClass('active').siblings('.obj').removeClass('active');
	});
	
	//.btn_top 
	$(window).scroll(function() { //.btn_top 
		if ($(this).scrollTop() > 400) {
			$('.pc .btn_top').fadeIn();
		} else {
			$('.pc .btn_top').fadeOut();
		}
		
		var footerH= $('footer').height(),
			scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();
		if (scrollBottom<footerH){//푸터만큼 밀어올림
			$('.pc .btn_top').css({
				'bottom':footerH+150-scrollBottom
			});
		}
	});
	$(window).resize(function(){//.btn_top 모바일 대응
		if($('html').hasClass('mob')){
			$('.btn_top').removeAttr('style');
		}
	})
	$(".pc .btn_top").click(function() {
		$('html, body').animate({
			scrollTop : 0
		}, 400);
	});
	
	//pop_layer
	if($('.pop_area').length){
		var $btn_close= $('.btn_pop_close');
		var $btn_pop= $('.fn_btn_pop');
		
		// 팝업 닫기
		$btn_close .on('click', function(){
			$('.pop_area') .removeAttr('style');
			$('.popup') .removeClass('active');
			$('html').removeAttr('style');
		});
		$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				$('.pop_area') .removeAttr('style');
				$('.popup') .removeClass('active');
				$('html').removeAttr('style');
			}
		});
		
		// 팝업 열기
		$btn_pop .on('click',function(e){
			e.preventDefault();
			var thisHref = $(this).attr("href");
			$(thisHref) .parents('.pop_area') .css('display','block');
			$(thisHref) .addClass('active').siblings().removeClass('active');
			$('html').css({
				'overflow':'hidden',
				'width':'100%',
				'height':'100%'
			});
		});
	}
	
	//etc
	if($('.main-tab_contents').length){//main-visual 각종 효과
		// 링크
		var $link= $('.main-tab_contents .link_area [class^="link_"]')
		$link.hover(function(){
			$(this).find('.obj_eff').stop().animate({'bottom':45}, 1000,'easeOutElastic');
		});
		$link.mouseleave(function(){
			$(this).find('.obj_eff').stop().animate({'bottom':36}, 500, 'easeOutBounce');
		});
		$link.hover(function(){
			$(this).find('.obj_eff.qmark').stop().animate({'top':-9}, 1000,'easeOutElastic');
		});
		$link.mouseleave(function(){
			$(this).find('.obj_eff.qmark').stop().animate({'top':0}, 500, 'easeOutBounce');
		});
	}
	if($('.btn_sns').length){//sns 공유
		$('.btn_sns') .on('click',function(){
			$(this).children('.close').toggle();
			$('.lnb .nav_area .sns_area') .unbind().slideToggle();
		});
	}
	if($('.sel_area').length){//select
		var selectTarget= $('.sel_area select');
		
		selectTarget.each(function(){
			var defaultText= $(this).children('option').first().text();
			$(this).siblings('label').text(defaultText);
		});
		
		selectTarget.change(function(){
			var select_name = $(this).children('option:selected').text();
			$(this).siblings('label').text(select_name);
		});
	}
	if($('.tbl_wrap_style_2').length){//.data_area 태이블 클릭
		$('.tbl_wrap_style_2 tbody.on_click tr').click(function(){
			$(this).addClass('active').siblings().removeClass('active');
		})
	}
	
	
	//navigation -gnb
	if($('.gnb').length){
		var $gnb= $('.gnb'),
			$s_menu= $('.gnb .s_menu_area');
		
		//pc
		if($('html').hasClass('pc')){
			$gnb.click(function(){//서브메뉴 열림
				$s_menu.unbind().slideDown().addClass('active');
			});
			$gnb.mouseleave(function(){//서브메뉴 닫힘
				$s_menu.unbind().slideUp().removeClass('active');
			});
			
			var lst_dep1= $('.gnb .dep1_area li'),
					arr_lst_dep1= lst_dep1.push();
			lst_dep1.hover(function(){
				var i= $(this).index()+1;
				$('.gnb .sub_menu'+i).css('background-color','#fff');
			})
			lst_dep1.mouseleave(function(){
				$('.gnb [class^="sub_menu"]').removeAttr('style');
			})
		}
		//mob
		if($('html').hasClass('mob')){
			var btn_menu= $('.mob .h_btn_area .btn_group_mob .btn_menu'),
				$dim= $('.mob .dim'),
				btn_dep1= $('.mob .gnb .obj_dep1'),
				btn_dep2= $('.mob .gnb .obj_dep2'),
				btn_dep3= $('.mob .gnb .obj_dep3');
			
			btn_menu .on('click',function(){
				$('body').toggleClass('fixed');
				$dim.toggle();
				$('.mob .h_wrap').toggleClass('gnb_on');
				$gnb.slideToggle('fast');
			});
			btn_dep1 .on('click',function(){
				$(this).parent('li.lst_dep1').addClass('active').siblings().removeClass('active');
			});
			
			btn_dep1.click(function(){
				var i= $(this).parent('li.lst_dep1').index()+1;
				$('.mob .gnb .sub_menu'+i).addClass('active').siblings().removeClass('active');
			});
			
			btn_dep3.on('click',function(){
				$('.mob .gnb .dep2_area').find('li.lst_dep3').removeClass('active');
				$(this).parent('li.lst_dep3').addClass('active');
			})
		}
	}
	
	//navigation -lnb
	if($('.lnb').length){
		var $btn_slide= $('.lnb .selected'),
			$dep1_area= $('.lnb .dep1_area'),
			$dep2_area= $('.lnb .dep2_area'),
			$dep3_area= $('.lnb .dep3_area'),
			$bar= $('.lnb .bar_type');
		
		//pc
		$btn_slide.on('click',function(){
			$(this).next().stop().slideDown();
		});
		$bar.mouseleave(function(){
			$(this).children('.fn_slideup').stop().slideUp();
		});
		
		for(i=1;i<7;i++){//.lnb_header에 지정된 .bg_menu_에 따라 lnb 메뉴 호출
			var $menu_no= $('.lnb .bg_menu_'+i),
				$tit_content= $('.lnb .title_content'),
				$tit_dep1= $('.lnb .mainmenu_area .selected_menu');
			
			if($menu_no.length){
				var $dep1_text= $dep1_area.children().eq(i-1).children().text(),
					$submenu= $('.lnb .sub_menu'+i);
				$tit_content.html($dep1_text);
				$tit_dep1.html($dep1_text);
				$submenu.addClass('active');
			}
		}
		//mob
		if($('.mob .lnb .dep2_area').length){
			var $obj= $('.mob .lnb .lst_dep2 [class^="obj_"]');
			
			$obj .on('click',function(){// 각 항목 활성화 : .active 부여
				$(this).parent('li.lst_dep2').toggleClass('active');
				$(this).parent('li.lst_dep3').addClass('active').siblings().removeClass('active');
				
				if($(this).siblings('ul').hasClass('dep3_area') && $(window).width()<1024){// 3뎁스 슬라이드
					$(this).siblings('.dep3_area').unbind().slideToggle('fast');
				}
			})
		}
		if($('.mob .lnb .dep3_area').length){// 2뎁스 하위에 서브메뉴 감지 후 아이콘클래스 부여
			$('.mob .lnb .dep3_area') .parent('.lst_dep2') .addClass('ico_slide')
		}
		$(window).resize(function(){// 가로 해상도 1024 이상의 디바이스에서 3뎁스 삭제
			if($(window).width()>1023){
				$('.lnb .dep3_area').removeAttr('style');
				$('.lnb .dep2_area [class*="lst_"]').removeClass('active');
			}
		})
	}
	
	//navigation -dep4_area
	if($('.mob nav.dep4_area').length){
		var tit= $('.mob nav.dep4_area .tabs .active').find('.btn_tab .align').text(),
			$tit= $('.mob nav.dep4_area .mob_tit .tit'),
			$btn_tab= $('.mob nav.dep4_area .btn_tab'),
			$btn_select= $('.mob nav.dep4_area .btn_select'),
			$tabs= $('.mob nav.dep4_area .tabs')
		;
		
		$tit.html(tit);
		
		$btn_tab.on('click',function(){
			var tit= $('.mob nav.dep4_area .tabs .active').find('.btn_tab .align').text();
			$tit.html(tit);
		})
		$btn_select.on('click',function(){
			$tabs.stop().slideDown();
		})
		$btn_select.mouseleave(function(){
			$tabs.stop().slideUp();
		})
		
	}
	
	

	
	//플러그인----------------------------------------
	//date-picker
	$.datepicker.setDefaults({
		dateFormat: 'yy-mm-dd',
		prevText: '이전 달',
		nextText: '다음 달',
		monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
		dayNames: ['일', '월', '화', '수', '목', '금', '토'],
		dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
		dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
		showMonthAfterYear: true,
		yearSuffix: '년',
		changeYear: true, 
		changeMonth: true 
	});

	


});// $ end
