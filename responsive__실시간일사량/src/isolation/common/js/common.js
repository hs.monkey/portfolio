(function(){ // 외부 라이브러리와 충돌을 막고자 모듈화.
// 브라우저 및 버전을 구하기 위한 변수들.
	'use strict';
	var agent = navigator.userAgent.toLowerCase(),
		name = navigator.appName,
		browser;

	// MS 계열 브라우저를 구분하기 위함.
	if(name === 'Microsoft Internet Explorer' || agent.indexOf('trident') > -1 || agent.indexOf('edge/') > -1) {
		browser = 'ie';
	} 
    else if(agent.indexOf('safari') > -1) { // Chrome or Safari
		if(agent.indexOf('opr') > -1) { // Opera
			browser = 'opera';
		} else if(agent.indexOf('chrome') > -1) { // Chrome
			browser = 'chrome';
		} else { // Safari
			browser = 'safari';
		}
	} else if(agent.indexOf('firefox') > -1) { // Firefox
		browser = 'firefox';
	}

	// IE: ie7~ie11, Edge, Chrome: chrome, Firefox: firefox, Safari: safari, Opera: opera
	document.getElementsByTagName('html')[0].className = browser;
}());
$(function () {//디바이스 체크
	var filter = 'win16|win32|win64|mac|macintel|linux i686';
	if(navigator.platform){
		if(filter.indexOf(navigator.platform.toLowerCase()) < 0){
			$('html').addClass('mob');
		}else{
			$('html').addClass('pc');
		}
	}
	if($('html').hasClass('mob')){
		if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
			$('html').addClass('ios');
		}else{
			$('html').addClass('android');
		}
	}
});
$(function () {// common	

//	todo 개발시 삭제 --pc화면에서 개발자 도구로 모바일 화면을 볼 때만 사용합니다.	
	var fn_reWinW = function(){
		if( $(window).width()>1366 ) {
			$('html').removeClass('mob').addClass('pc');
		}else{
			$('html').removeClass('pc').addClass('mob');
		}
	}
	fn_reWinW();
	$(window).resize(function(){
		fn_reWinW();
	});
	//---/---
	
	//텝 메뉴
	if ($('.tabs').length){
		$('.tabs .btn-tab').on('click', function (e) {
			$(this).parent('li').addClass('active').siblings().removeClass('active');
		});
		if ($('.tabContents').length){
			$('.tabs .btn-tab[href*="tabCont-"]').on('click', function (e) {
				e.preventDefault();
				var thisHref = $(this).attr('href');
				$(thisHref).addClass('active').siblings().removeClass('active');
			});
		}
	}

	//slide
	if($('.slide_area').length){
		$('.slide_area .btn_slide').on('click',function(){
			$(this).parent('li').toggleClass('active');
			$(this).siblings('.cont_slide').stop().slideToggle();
		})
	}
	
	//paging
	$('.paging_area .obj').on('click',function(){
		$(this).addClass('active').siblings('.obj').removeClass('active');
	});
	
	//.btn_top 
	$(window).scroll(function() { //.btn_top 
		if ($(this).scrollTop() > 100) {
			$('.btn_top').fadeIn();
		} else {
			$('.btn_top').fadeOut();
		}
	});
	$(window).resize(function(){//.btn_top 모바일 대응
		if($('html').hasClass('mob')){
			$('.btn_top').removeAttr('style');
		}
	})
	$(".pc .btn_top").click(function() {
		$('html, body').animate({
			scrollTop : 0
		}, 400);
	});
	$(".btn_top").click(function() {
		$('html, body').animate({
			scrollTop : 0
		}, 200);
	});
	
	//레이어 팝업
	var $btn_pop = $('.btn-popup'),
		$btn_pop_close = $('.btn-pop-close, .btn-pop-confirm'),
		popLayer_wrap = '.popLayer-wrap';

	$btn_pop .on('click',function(e){
		e.preventDefault();
		gnbClose();
		var thisHref = $(this).attr("href");
		$(thisHref).removeClass('hide') .parents(popLayer_wrap) .fadeIn('fast');
		$('html').addClass('lock');
	});

	function popClose(){ // popup 닫기
		$(popLayer_wrap) .hide() .find('.section') .addClass('hide');
		$('html').removeClass('lock');
	}

	$btn_pop_close .on('click',function(){// popup 닫기 버튼 클릭
		popClose();
	})
	$(document).keyup(function(e) {// ESC 눌러 닫기
		if (e.keyCode == 27) {
			popClose();
		}
	});
	
});//end  common


$(function(){
    gnb();
});//end figure

function gnb(){
    $('.mob .btn-gnb').on('click', function(){
        $('.mob .menulist-wrap').stop().slideToggle(300);
    });
};

$(function(){
	$('#click').click();//태스트용 클릭 이벤트
});