$(function () {

	var $html = $('html');
	var win = $(window);
	var winW = win.width();
	var winH = win.height();
	var reWidth = $(window).width();
	var layer_btn; // 레이어팝업 포커스 백업 키

	//resize 함수 정리
	win.resize(function(){
		winW = $(window).width();
		setTimeout(function(){
			engMainSlider(); //메인슬라이드 768 ~ 1024 높이제어
		}, 600);
	});



	var $list = $('.main_slide_list'),
		$img = $('.main_slide_img');

	$img.find('ul').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.main_slide_list ul',
		draggable :false,
		infinite: false,
		responsive: [
							{
								breakpoint: 1024,
								settings: {
											draggable: true,
											}
							}
						]
	});

	$list.find('ul').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.main_slide_img ul',
		dots: false,
		draggable :false,
		vertical:true,
		arrows: true,
		infinite: false,
		responsive: [
							{
							  breakpoint: 1024,
							  settings: {
											slidesToShow: 3,
											slidesToScroll: 1,
											draggable: true,
											verticalSwiping:true,
										  }
							},
							{
							  breakpoint: 870,
							  settings: {
											slidesToShow: 2,
											slidesToScroll: 1,
											draggable: true,
											verticalSwiping:true,
										  }
							},
							{
							  breakpoint: 769,
							  settings: {
											slidesToShow: 1,
											slidesToScroll: 1,
											draggable: true,
											vertical:false,
											verticalSwiping:false,
											adaptiveHeight:true,
											infinite: true,
										  }
							},
						]
		});

	function engMainSlider(){
		if(winW <= 1024 && winW >= 768){
			$list.height($img.height());
			$list.find('ul').height($img.height());
		}else if (winW > 1024){
			$list.height(520);
		}
	}engMainSlider();



	$list.find('li').each(function(i){
		$(this).mouseenter(function(){
			$img.find('li').eq($(this).index()).find('a').clone().appendTo('.hidden_img')
		}).mouseleave(function(){
			$('.hidden_img').find('a').remove()
		})
	});

	$('.link_banner ul').slick({
		dots: true,
		arrows: false,
		autoplay:true,
		responsive: [
							{
							  breakpoint: 768,
							  settings: {
											slidesToShow: 2,
											slidesToScroll: 1,
										  }
							}
						]
	});

	$('.link_banner').append('<span class="stop"><button class="btn-slider-stop">stop</button></span><span class="play"><button class="btn-slider-play" style="display:none">play</button></span>');
	$(document).on('click','.link_banner .btn-slider-stop',function(){
		var link_banner = $('.link_banner');
		var link_banner_list = $('.link_banner ul');
		link_banner.find('.btn-slider-play').show();
		$(this).hide();
		link_banner_list.slick('slickPause');
	}).on('click','.link_banner .btn-slider-play',function(){
		var link_banner = $('.link_banner');
		var link_banner_list = $('.link_banner ul');
		link_banner.find('.btn-slider-stop').show();
		$(this).hide();
		link_banner_list.slick('slickPlay');
	});

	//메인 공통 ie8 background Size
	$('.main_body').css({backgroundSize: "cover"});

	//메인 공지 리스트
	$('.news_board > .tabs > li > a').on('click focusin', function(){
		var that = $(this);
		$('.board_body').hide();
		that.next('.board_body').show();
	});
	$('.news_board > .tabs > li').eq(0).find('> a').trigger('click');


});// $ end