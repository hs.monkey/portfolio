$(function () {

	var win = $(window);
	var winW = win.width();
	var winH = win.height();
	var reWidth = $(window).width();
	var layer_btn; // 레이어팝업 키

	//resize 함수 정리
	win.resize(function(){
		winW = $(window).width();

		setTimeout(function(){
			totalSearch(); //통합검색
			mainGnb();
		}, 500);
	});


	//디바이스 체크
	var filter = 'win16|win32|win64|mac|macintel|linux i686';
	if(navigator.platform){
		if(filter.indexOf(navigator.platform.toLowerCase()) < 0){
			$('html').addClass('mob');
		}else{
			$('html').addClass('pc');
		}
	}
	if($('html').hasClass('mob')){
		if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
			$('html').addClass('ios');
		}else{
			$('html').addClass('android');
		}
	}



	//GNB
	var gnb_wrap_inner = $('.gnb_wrap_inner');
	var gnb = $('.gnb');
	var gnbLi = gnb.find('> li');
	var gnbLiA = gnbLi.find('> a');
	var gSub = $('.g_sub');
	var $headerBg = $('.hbg');

	function mainGnb(){
		var win = $(window);
		var winW = win.width();
		gnbLiA.off('.gnbpc');
		gnbLiA.off('.mobClick');
		gnbLi.off('.gnbpc');

		if(winW > 1024){
			gnb.height('auto');
			gnb_wrap_inner.css({'right':0}).show();
			$('.menu_close').hide();

			gnbLiA.on('mouseover.gnbpc focusin.gnbpc', function(){
				var $self = $(this);
				var $parent = $self.parent(); // li

				$self.addClass('hover');
				$parent.siblings().find('>a').removeClass('hover');

				gSub.hide();
				$parent.find(gSub).show();

				$('.gnb .g_sub_list2').masonry({
					itemSelector: '.unit'
				});

				$('.total_search p').addClass('z_down');
				$headerBg.show().stop().css({height:$parent.find('.g_sub_list2').outerHeight()}, 'fast');
				
			}).parent().on('mouseleave.gnbpc', function(){

				var $self = $(this);
				$self.find('>a').removeClass('hover');
				gSub.hide();
				$headerBg.hide();
				$('.gnb .g_sub_list2').masonry('destroy');
				$('.total_search p').removeClass('z_down');

			});

			$('.unit').last().on('focusout', function(){
				gnbLiA.removeClass('hover');
				gSub.hide();
				$headerBg.hide();
			})


		}else{


			//$('.unit, .g_sub_list2').attr('style', '');

			gnb.height(win.height());//모바일 메뉴 디바이스 해상도 높이

			gnbLiA.on('click.mobClick', function(){
				var $self = $(this);
				var $parent = $self.parent(); // li
				$parent.siblings().find('a').removeClass('on');

				gSub.hide();
				if(!$self.hasClass('on')){
					$self.addClass('on').attr('title','하위 메뉴열림');
					$parent.find(gSub).show();
				}else{
					$self.removeClass('on').attr('title','하위 메뉴닫힘');
					$parent.find(gSub).hide();
				}
			});


			$('.g_sub_list2 > li > strong').on('click.mobclick', function(){
				if($(this).next().is('.g_sub_list3')){
					var that = $(this);
					var thatChild = that.find('a');
					if(!thatChild.hasClass('on')){
						thatChild.addClass('on');
						thatChild.attr('title','하위 메뉴열림');
						that.next('.g_sub_list3').show();
					}else{
						thatChild.removeClass('on');
						thatChild.attr('title','하위 메뉴닫림');
						that.next('.g_sub_list3').hide();
					}
				}
			});

		} //  if end

	}mainGnb();
	
	$(window).resize(function(){
		reWidth2 = $(document).width();
		if(reWidth<1024){
			if(reWidth2>1024){
				reWidth = reWidth2;
				window.location.reload();
			}
		}else {
			if(reWidth2<1024){
				reWidth = reWidth2;
				window.location.reload();
			}
		}
	});


	//모바일 GNB 열기 설정
	$('.mob_menu button').on('click', function(){
		gnb_wrap_inner.show().stop().animate({right:0});
		$headerBg.stop().fadeIn();
		$('.menu_close').stop().fadeIn();
		$('.total_search p').addClass('z_down');
	});

	//모바일 GNB 닫기 설정
	$('.menu_close, .hbg').on('click', function(){
		gnb_wrap_inner.stop().animate({right:(-70+'%')},300 , function(){
			$(this).hide();
			$('.total_search p').removeClass('z_down');
		});
		$headerBg.stop().fadeOut();
		$('.menu_close').stop().fadeOut();
	});

	//전체메뉴 열기 설정
	$('.allmenu').on('click', function(){
		gnbLiA.removeClass('hover');
		$(this).toggleClass('on');
		$('.total_search p').addClass('z_down');
		$headerBg.hide();
		if($(this).hasClass('on')){
			$(this).find('a').attr('title', '메뉴선택됨 전체메뉴 열림');
			gSub.hide();
			gnbLiA.off('.gnbpc'); //전체 메뉴 열림상태시 gnb pc 미적용
		}else{
			$(this).find('a').attr('title', '전체메뉴 닫힘');
			mainGnb();
		}
		$('.all_gnb').toggle(0);
	});


	$('.all_gnb .dep2_title').each(function(i){
		if($(this).next('ul').length <= 0){
			$(this).find('a').css({'background-image':'none'});
		}
		$(this).find('> a').on('click', function(e){
			e.preventDefault();
			$('.dep2').stop().slideUp();
			$(this).parent().next('.dep2').stop().slideToggle();
		});
	});

	//전체메뉴 닫힘
	$('.all_menu_close').click(function(){
		$('.dep2').stop().slideUp(0);
		$('.all_gnb').hide();
		$('.allmenu').find('a').attr('title', '전체메뉴 닫힘');
		mainGnb();
	}).on('focusout', function(){ //전체메뉴버튼 포커스 아웃시 설정
		$('.dep2').stop().slideUp(0);
		$('.all_gnb').hide();
		$('.allmenu').find('a').attr('title', '전체메뉴 닫힘');
	});

	//gnb 메뉴에 target 속성있을시 bg 추가
	$('.mob_gnb_wrap, .all_gnb .dep2').find('a').each(function(){
		if($(this).attr('target')){
			$(this).addClass('blank');
		}
	});


	//탭 메뉴 접근성 강화
	//.tabs  공통 클래스 사용
	var clickEvent = $('.tabs > li');
	var tabOnText = '메뉴선택됨';
	$.fn.clickOn = function(){
		$(this).on('click focusin', function(){
			var with_siblings = $(this).siblings();
			with_siblings.removeClass('on').find(' > a').removeAttr('title');
			$(this).addClass("on");
			$(this).find(' > a').attr({'title':tabOnText, 'role':'button'});
		});
	};
	$(clickEvent).clickOn();
	$(clickEvent).each(function(){
		$(this).find(' > a').attr({'title':tabOnText});
	});

	//푸터베너
	$('.foot_banner ul').slick({
		infinite: true,
		speed: 300,
		slidesToShow: 8,
		slidesToScroll: 1,
		arrows:true,
		draggable:false,
		autoplay:true,
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 6,
				slidesToScroll: 1,
				draggable:true,
				arrows:false,
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows:false,
				draggable:true
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows:false,
				draggable:true
			  }
			},
		]
	});


	$('.foot_banner > div').append('<span class="stop"><button class="btn-slider-stop">정지</button></span><span class="play"><button class="btn-slider-play" style="display:none">재생</button></span>');
	$(document).on('click','.foot_banner .btn-slider-stop',function(){
		var foot_wrap = $('.foot_banner > div');
		var foot_bn = $('.foot_banner ul');
		foot_wrap.find('.btn-slider-play').show();
		$(this).hide();
		foot_bn.slick('slickPause');
	}).on('click','.foot_banner .btn-slider-play',function(){
		var foot_wrap = $('.foot_banner > div');
		var foot_bn = $('.foot_banner ul');
		foot_wrap.find('.btn-slider-stop').show();
		$(this).hide();
		foot_bn.slick('slickPlay');
	});


	//통합검색 해상도 온오프
	function totalSearch(){
		if(winW >1024 && $('body').hasClass('main')){
			$('.total_search').hide();
		}
	}totalSearch();


	//모바일 소셜공유
	$('.share button').click(function(){
		$(this).next('span').toggle(0);
	});
	$('.share .aria_chk').click(function(){
		$('.share span').toggle(0);
	});



	// 푸터 기관노출
	$('.slot button').on('click', function(i){
		var aria = $(this).find('.aria_chk');
		var ariaText = aria.text();
		$(this).parents('.slot').find('div').toggle(0, function(){
			aria.text(ariaText == "링크메뉴닫힘" ? "링크메뉴열림" : "링크메뉴닫힘");
		});
	});


	$('.ins_control a').on('click', function(){
		$(this).toggleClass('on');
		if($(this).attr('title') == "상세메뉴닫힘"){
			$(this).attr('title', "상세메뉴열림");
		}else{
			$(this).attr('title', "상세메뉴닫힘");
		}
		$('.detail_search_box').toggle(0);
	});



	//연관컨텐츠
	var thum_scroll_wrap = $('.thum_scroll_wrap');
	var thum_list = $('.thum_list');
	thum_list.find('li').each(function(i){
		var w = $(this).outerWidth(true) * (i+1)
		thum_list.width(w)
	});


	$('.sort_select .current').on('click', function(){
		var that = $(this);
		that.next().stop().slideToggle();
		that.next('ul').find('a').click(function(){
			var text = $(this).text();
			that.text(text);
			$(this).parents('ul').stop().slideUp();
		});
	})
	


});// $ end