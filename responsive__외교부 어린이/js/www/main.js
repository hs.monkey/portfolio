$(function () {

	var win = $(window);
	var winW = win.width();
	var winH = win.height();
	var reWidth = $(window).width();
	var layer_btn; // 레이어팝업 키

	//resize 함수 정리
	win.resize(function(){
		winW = $(window).width();
		setTimeout(function(){
			mainSlickBtnPosition(); //메인 슬라이딩 좌우버튼 위치조절 호출
			//mama(); //#packaging02 masonry
		}, 500);
		tourImg(); //해외순방
	});

	//메인전체영역 슬라이드
	$('.main_body').slick({
		//dots:true,
		arrows:true,
		infinite:false,
		autoplay:false,
			/*
		customPaging : function(slider, i) {
			var dtitle = $(slider.$slides[i]).data('title');
			return '<button class="slider-button-title">'+dtitle+'</button>';
		},
		*/
		accessibility:true,
		slidesToShow: 1,
		focusOnSelect:true,
		adaptiveHeight:true,
		asNavFor: '.dot_action',
		swipe:false,
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				draggable:true,
				swipe:true
			  }
			}
		]
	}).on('beforeChange', function(event, slick, currentSlide,  nextSlide){
		// 현재 선택된 slick slide 를 제외한 나머지 slide 내의 모든 링크에 tabindex=-1 부여
		$('.main_body div[id^=packaging0]').each(function(){
			if ($(this).hasClass('slick-current')){
				$(this).find('a,area,button,input[type=submit],input[type=image]').attr('tabindex','0');
			} else {
				$(this).find('a,area,button,input[type=submit],input[type=image]').attr('tabindex','-1');
			}
		});
	});



	$(document).on('click','.main_body .slider-button-title', function(){
		var idx = $(this).parent().index();
		$('.main_body').slick('slickGoTo', idx-1);
		$('.main_body div[id^=packaging0]').eq(idx).focus();
	});


	$('.dot_action').slick({
		arrows:false,
		infinite:false,
		dots:false,
		swipe:false,
		slidesToShow: 3,
		slidesToScroll: 1
	});



	$('a[data-slide]').click(function(e) {
		e.preventDefault();
		var idx = $(this).parent().index();
		var slideno = $(this).data('slide');
		$('.main_body').slick('slickGoTo', slideno - 1);
		$(this).attr('title', '선택됨');

		//이중 slick 포커스 실종 현상 방지
		setTimeout(function () {
			$('.main_body div[id^=packaging0]').eq(slideno - 1).focus();
		},500);
		$(".main_map_box").mapImg();
 	});

	$(document).on('click','.main_body > .slick-arrow',function(){
		$(".main_map_box").mapImg();
		$('div[id^="packaging0"]').each(function(){
			if($(this).hasClass('slick-active')){
				var idxx = $(this).index();
				$('.main_body').slick('slickGoTo', idxx);
				setTimeout(function () {
					$('div[id^="packaging0"]').eq(idxx-1).focus();
				},500);
			}
		})
	});



	//메인 스탭1 new 슬라이드
	$('.news_bn ul').slick({
		infinite:false,
		autoplay:true,
		dots:true,
		fade:true,
		arrows:true,
		pauseOnHover:false,
	});

	$('.quick_bn1 ul').slick({ autoplay:true, dots:true, arrows:false });
	$('.quick_bn2 ul.bbg').slick({ autoplay:true, dots:true, arrows:true });
	$('.quick_bn2 ul.sml').slick({ autoplay:false, dots:false, arrows:true });
	function tourImg(){
		if(winW <= 1024 && winW >= 480){
			var h = $('.quick_bn1').height() - 14 - $('.quick_bn2 .sml').innerHeight();
			$('.president_tour a').height(h);
			$('.president_tour a').css({'line-height':(h + 4)+'px'});
		}
	}tourImg();


	$('.news_bn .slick-dots').append('<li class="stop"><button class="btn-slider-stop">정지</button></li><li class="play"><button class="btn-slider-play" style="display:none">재생</button></li>');
	$(document).on('click','.news_bn .btn-slider-stop',function(){
		var news_wrap = $('.news_bn .slick-dots');
		var news_bn = $('.news_bn > ul');
		news_wrap.find('.btn-slider-play').show();
		$(this).hide();
		news_bn.slick('slickPause');
	}).on('click','.news_bn .btn-slider-play',function(){
		var news_wrap = $('.news_bn .slick-dots');
		var news_bn = $('.news_bn > ul');
		news_wrap.find('.btn-slider-stop').show();
		$(this).hide();
		news_bn.slick('slickPlay');
	});


	$('.quick_bn2 .slick-dots').append('<li class="stop"><button class="btn-slider-stop">정지</button></li><li class="play"><button class="btn-slider-play" style="display:none">재생</button></li>');
	$(document).on('click','.quick_bn2 .btn-slider-stop',function(){
		var quick_wrap = $('.quick_bn2 .slick-dots');
		var quick_bn = $('.quick_bn2 > ul');
		quick_wrap.find('.btn-slider-play').show();
		$(this).hide();
		quick_bn.slick('slickPause');
	}).on('click','.quick_bn2 .btn-slider-play',function(){
		var quick_wrap = $('.quick_bn2 .slick-dots');
		var quick_bn = $('.quick_bn2 > ul');
		quick_wrap.find('.btn-slider-stop').show();
		$(this).hide();
		quick_bn.slick('slickPlay');
	});



	//#packaging02 masonry 세팅
/*
	var masonryOptions = {
		itemSelector: '.item',
		columnWidth: 227,
		gutter:17,
	};
*/
/*
	var $colum = $('.colum ul').masonry( masonryOptions );
	function mama(){
		var winW = $(window).width();
		if(winW < 1024){
			$('.colum').addClass('destroy');

			if($('.colum').hasClass('destroy')){
				$colum.masonry('destroy');
			}
		}else{
			$colum.masonry(masonryOptions);
		}
	}mama();
*/

	//#packaging02 masonry 세팅
	if(winW > 1024){
		$('.colum ul').masonry({
			itemSelector: '.item',
			columnWidth: 227,
			gutter:17
		});
	}else{
		$('.colum ul').masonry('destroy');
	}


	//메인 공통 1 Depth 슬라이드 터치 버블링 방지
	$('.noreturn').on("mousedown mouseup touchmove", function(event) {
	  event.stopPropagation();
	});

	//메인 공통 ie8 background Size
	var ie8Bgz = ['.main_body div[id^=packaging]', '.sub01', '.sub02'];
	$(ie8Bgz.join()).css({backgroundSize: "cover"});

	//메인 스탭1 공지사항 탭
	$('.news_board > .tabs > li > a').on('click focusin', function(){
		var that = $(this);
		$('.board_body').hide();
		that.next('.board_body').show();
	});
	$('.news_board > .tabs > li').eq(0).find('> a').trigger('click');


	//메인 슬라이딩 좌우버튼 위치조절
	function mainSlickBtnPosition(){
		var winW = $(window).width();
		var inW = $('.in').width();
		var mainBody = $('.main_body');
		var mainArrW = $('.main_body > .slick-arrow').width() + 75; //padding 75;
		var left = mainBody.find('> .slick-prev');
		var right = mainBody.find('> .slick-next');
		var ratio = ((winW-inW)/2);

		if(winW < 1500){
			left.css({left:ratio * 0.2});
			right.css({right:ratio * 0.2});
		}else{
			left.css({left:ratio - mainArrW});
			right.css({right:ratio - mainArrW});
		}
	}
	mainSlickBtnPosition();



	//메인검색
	/*
	var search_input = $('.search_input'),
		thatP = search_input.parent('p'),
		thatX = thatP.offset().left,
		thatY = thatP.offset().top,
		thatW = thatP.width(),
		thatH = thatP.height(),
		new_W = (thatW * 2),
		posLeft = thatX - ((new_W - thatW)/2);

		var isChrome = !!window.chrome;
		var isIE = false || !!document.documentMode;



	var main_content_top = $('.main_content_top');

	search_input.click(function(){
		if(main_content_top.hasClass('active')){

			thatP.animate({left:thatX, top:thatY, width:thatW} , function(){
				main_content_top.removeClass('active')
				thatP.css({left:0, top:0, width:425})
			});

		}else{
			main_content_top.addClass('active')
			thatP.css({left:thatX, top:thatY}).stop().animate({left:posLeft, top:(winH - thatH)/2, width:new_W},630 ,'easeInOutQuart');
		}

	})

	*/


	//#packaging03 지도
	$.fn.mapImg = function(options) {
		return this.each(function() {
			var self = this,
			$self = $(self),
			$map = $self.find("map"),
			$item = $map.find("area");

			$item.each(function(i) {
				var item = this,
				$item = $(item),
				contentId = item.getAttribute('href', 2),
				content = contentId && (contentId !== '#') && $(document).find(contentId).addClass("map_content");
				image = $(".map_content");

				$.data(item, "i.mapImg", i);

				$item.bind("mouseenter focusin", function() {
					select(i);
				});

				image.bind("mouseleave", function() {
					unselet();
				});
				$item.bind("focusout", function() {
					unselet();
				});

				select();

				function select(i) {
					if(i < 0) {
						return null;
					}
					var selected = image.eq(i);
					selected.find('a').attr("title", "선택됨");
					selected.addClass("map_content_selected");
				}

				function unselet() {
					var selected = image;

					selected.find('a').attr("title", "");
					selected.removeClass("map_content_selected");
				}
			});
		});
	};


});// $ end