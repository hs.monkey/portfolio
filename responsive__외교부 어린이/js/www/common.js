$(function () {

	var win = $(window);
	var winW = win.width();
	var winH = win.height();
	var reWidth = $(window).width();
	var layer_btn; // 레이어팝업 키

	//resize 함수 정리
	win.resize(function(){
		winW = $(window).width();

		setTimeout(function(){
			totalSearch(); //통합검색
			worldDocDt(); //주한공관주소록 높이
			mainGnb();
			popResize(); //레이어팝업 리사이징 (국가정보)
			orgChart(); //조직도
		}, 500);
	});


	//디바이스 체크
	var filter = 'win16|win32|win64|mac|macintel|linux i686';
	if(navigator.platform){
		if(filter.indexOf(navigator.platform.toLowerCase()) < 0){
			$('html').addClass('mob');
		}else{
			$('html').addClass('pc');
		}
	}
	if($('html').hasClass('mob')){
		if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
			$('html').addClass('ios');
		}else{
			$('html').addClass('android');
		}
	}



	//GNB
	var gnb_wrap_inner = $('.gnb_wrap_inner');
	var gnb = $('.gnb');
	var gnbLi = gnb.find('> li');
	var gnbLiA = gnbLi.find('> a');
	var gSub = $('.g_sub');
	var $headerBg = $('.hbg');

	function mainGnb(){
		var win = $(window);
		var winW = win.width();
		gnbLiA.off('.gnbpc');
		gnbLiA.off('.mobClick');
		gnbLi.off('.gnbpc');

		if(winW > 1024){
			gnb.height('auto');
			gnb_wrap_inner.css({'right':0}).show();
			$('.menu_close').hide();

			gnbLiA.on('mouseover.gnbpc focusin.gnbpc', function(){
				var $self = $(this);
				var $parent = $self.parent(); // li

				$self.addClass('hover');
				$parent.siblings().find('>a').removeClass('hover');

				gSub.hide();
				$parent.find(gSub).show();

				$('.gnb .g_sub_list2').masonry({
					itemSelector: '.unit'
				});

				$('.total_search p').addClass('z_down');
				$headerBg.show().stop().css({height:$parent.find('.g_sub_list2').outerHeight()}, 'fast');
				
			}).parent().on('mouseleave.gnbpc', function(){

				var $self = $(this);
				$self.find('>a').removeClass('hover');
				gSub.hide();
				$headerBg.hide();
				$('.gnb .g_sub_list2').masonry('destroy');
				$('.total_search p').removeClass('z_down');

			});

			$('.unit').last().on('focusout', function(){
				gnbLiA.removeClass('hover');
				gSub.hide();
				$headerBg.hide();
			})


		}else{


			//$('.unit, .g_sub_list2').attr('style', '');

			//gnb.height(win.height());//모바일 메뉴 디바이스 해상도 높이

			gnbLiA.each(function(){
				if($(this).hasClass('on')){
					$(this).attr('title','하위 메뉴열림');
					$(this).parent().find(gSub).show();
				}
			}).on('click.mobClick', function(){
				var $self = $(this);
				var $parent = $self.parent(); // li
				$parent.siblings().find('a').removeClass('on');

				gSub.hide();
				if(!$self.hasClass('on')){
					$self.addClass('on').attr('title','하위 메뉴열림');
					$parent.find(gSub).show();
				}else{
					$self.removeClass('on').attr('title','하위 메뉴닫힘');
					$parent.find(gSub).hide();
				}
			});


			$('.g_sub_list2 > li > strong > a').on('click.mobclick', function(){
				if($(this).parent().next().is('.g_sub_list3')){
					var that = $(this).parent();
					var thatChild = that.parent().find('a');
					if(!thatChild.hasClass('on')){
						thatChild.addClass('on');
						thatChild.attr('title','하위 메뉴열림');
						that.next('.g_sub_list3').show();
					}else{
						thatChild.removeClass('on');
						thatChild.attr('title','하위 메뉴닫힘');
						that.next('.g_sub_list3').hide();
					}
				}
			});

		} //  if end

	}mainGnb();
	

	$(window).resize(function(){
		reWidth2 = $(document).width();
		if(reWidth <= 1024){
			if(reWidth2 >= 1024){
				reWidth = reWidth2;
				window.location.reload();
			}
		}else {
			if(reWidth2 <= 1024){
				reWidth = reWidth2;
				window.location.reload();
			}
		}
	});


	//모바일 GNB 열기 설정
	$('.mob_menu button').on('click', function(){
		gnb_wrap_inner.show().stop().animate({right:0});
		$headerBg.stop().fadeIn();
		$('.menu_close').stop().fadeIn();
		$('.total_search p').addClass('z_down');
	});

	//모바일 GNB 닫기 설정
	$('.menu_close, .hbg').on('click', function(){
		gnb_wrap_inner.stop().animate({right:(-70+'%')},300 , function(){
			$(this).hide();
			$('.total_search p').removeClass('z_down');
		});
		$headerBg.stop().fadeOut();
		$('.menu_close').stop().fadeOut();
	});

	//전체메뉴 열기 설정
	$('.allmenu').on('click', function(){
		gnbLiA.removeClass('hover');
		$(this).toggleClass('on');
		$('.total_search p').addClass('z_down');
		$headerBg.hide();
		if($(this).hasClass('on')){
			$(this).find('a').attr('title', '메뉴선택됨 전체메뉴 열림');
			gSub.hide();
			gnbLiA.off('.gnbpc'); //전체 메뉴 열림상태시 gnb pc 미적용
		}else{
			$(this).find('a').attr('title', '전체메뉴 닫힘');
			mainGnb();
		}
		$('.all_gnb').toggle(0);
	});


	//PC gnb 설정
	/*
	function gnbPc(){
		$gnbItem.on('mouseover.gnbpc focusin.gnbpc', function(){
			var $self = $(this).parent('li');
			$self.find('>a').addClass('hover');
			$self.siblings().find('>a').removeClass('hover');

			$g_sub.hide();
			$self.find('.g_sub').show();
			$self.find('.g_sub_list2').show();
			$('.total_search p').addClass('z_down');
			$('.gnb .g_sub_list2').masonry({
				itemSelector: '.unit'
			});
			$headerBg.show().stop().css({height:$self.find('.g_sub_list2').outerHeight()}, 'fast');

		}).parent().on('mouseleave.gnbpc', function(){
			var $self = $(this);
			$gnbItem.removeClass('hover');
			$g_sub.hide();
			$headerBg.hide();
			$('.total_search p').removeClass('z_down');

		}).on('blur focusout', function(){
			var $self = $(this).parent('li');
			$self.find('>a').removeClass('hover');
			$('.total_search p').addClass('z_down');
		});

		$('.unit').last().on('focusout', function(){
			$gnbItem.removeClass('hover');
			$g_sub.hide();
			$headerBg.hide();
		})

	}
	gnbPc();
	*/

	//모바일 GNB 설정
	/*
	function gnbMobile(){
		$m_gnbItem2.removeClass('on');
		$m_gnbItem3.find('a').removeClass('on');
		$('.mob_gnb .g_sub_list2').hide();
		$('.mob_gnb .g_sub_list3').hide();

		$m_gnbItem2.on('click.mobclick', function(){
			var that = $(this);
			if(!that.hasClass('on')){
				that.addClass('on').attr('title','하위 메뉴열림');
				//that.attr('title','하위 메뉴열림');
				that.parent().find('.g_sub_list2').show();
			}else{
				that.removeClass('on').attr('title','하위 메뉴닫힘');
				//that.attr('title','하위 메뉴닫힘');
				that.parent().find('.g_sub_list2').hide();
			}
		});

		$m_gnbItem3.on('click.mobclick', function(){
			if($(this).next().is('.g_sub_list3')){
				var that = $(this);
				if(!that.find('a').hasClass('on')){
					that.find('a').addClass('on');
					that.find('a').attr('title','하위 메뉴열림');
					that.next('.g_sub_list3').show();
				}else{
					that.find('a').removeClass('on');
					that.find('a').attr('title','하위 메뉴닫힘');
					that.next('.g_sub_list3').hide();
				}
			}
		});
	}gnbMobile();
	*/

	$('.all_gnb .dep2_title').each(function(i){
		if($(this).next('ul').length <= 0){
			$(this).find('a').css({'background-image':'none'});
		}
		$(this).find('> a').on('click', function(e){
			e.preventDefault();
			$('.dep2').stop().slideUp();
			$(this).parent().next('.dep2').stop().slideToggle();
		});
	});

	//전체메뉴 닫힘
	$('.all_menu_close').click(function(){
		$('.dep2').stop().slideUp(0);
		$('.all_gnb').hide();
		$('.allmenu').find('a').attr('title', '전체메뉴 닫힘');
		mainGnb();
	}).on('focusout', function(){ //전체메뉴버튼 포커스 아웃시 설정
		$('.dep2').stop().slideUp(0);
		$('.all_gnb').hide();
		$('.allmenu').find('a').attr('title', '전체메뉴 닫힘');
	});

	//gnb 메뉴에 target 속성있을시 bg 추가
	$('.mob_gnb_wrap, .all_gnb .dep2').find('a').each(function(){
		if($(this).attr('target')){
			$(this).addClass('blank');
		}
	});


	//탭 메뉴 접근성 강화
	//.tabs  공통 클래스 사용
	var clickEvent = $('.tabs > li');
	var tabOnText = '메뉴선택됨';
	$.fn.clickOn = function(){
		$(this).on('click focusin', function(){
			var with_siblings = $(this).siblings();
			with_siblings.removeClass('on').find(' > a').removeAttr('title');
			$(this).addClass("on");
			$(this).find(' > a').attr({'title':tabOnText, 'role':'button'});
		});
	};
	$(clickEvent).clickOn();
	$(clickEvent).each(function(){
		$(this).find(' > a').attr({'title':tabOnText});
	});

	//푸터베너
	$('.foot_banner ul').slick({
		infinite: true,
		speed: 300,
		slidesToShow: 8,
		slidesToScroll: 1,
		arrows:true,
		draggable:false,
		autoplay:true,
		responsive: [
			{
			  breakpoint: 1024,
			  settings: {
				slidesToShow: 6,
				slidesToScroll: 1,
				draggable:true,
				arrows:false,
				autoplay:false,
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: 4,
				slidesToScroll: 1,
				arrows:false,
				draggable:true
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 2,
				slidesToScroll: 1,
				arrows:false,
				draggable:true
			  }
			},
		]
	});


	$('.foot_banner > div').append('<span class="stop"><button class="btn-slider-stop">정지</button></span><span class="play"><button class="btn-slider-play" style="display:none">재생</button></span>');
	$(document).on('click','.foot_banner .btn-slider-stop',function(){
		var foot_wrap = $('.foot_banner > div');
		var foot_bn = $('.foot_banner ul');
		foot_wrap.find('.btn-slider-play').show();
		$(this).hide();
		foot_bn.slick('slickPause');
	}).on('click','.foot_banner .btn-slider-play',function(){
		var foot_wrap = $('.foot_banner > div');
		var foot_bn = $('.foot_banner ul');
		foot_wrap.find('.btn-slider-stop').show();
		$(this).hide();
		foot_bn.slick('slickPlay');
	});


	// lnb
	var openText = '하위 메뉴 열림';
	var closeText = '하위 메뉴 닫힘';
	$('.sub_lnb ul ul li').each(function(){
		if($(this).is('.on')){
			$(this).parent('ul').show();
			$(this).parent('ul').prev().find('.aria_chk').text(openText);
			$(this).parent().prev().addClass('on')
		}
	});

	$('.sub_lnb > ul > li').each(function(){
		if($(this).find('ul').size() > 0){
			$(this).addClass('use_menu')
		}
	});


	$('.sub_lnb strong').on('click', function(){
		var self = $(this);
		var ariaChkText = self.find('.aria_chk');

		self.find('span').text(ariaChkText.text() == openText ? closeText : openText);
		self.parent('li').siblings().find('.aria_chk').text(closeText);

		$('.sub_lnb li ul').stop().slideUp();
		self.next('ul').stop().slideToggle();
		self.parent('li').siblings('li').find('> strong').removeClass('on');

		if(self.hasClass('on')){
			self.removeClass('on');
		}else{
			self.addClass('on');
		}
	});
	$('.sub_lnb').find('a').each(function(){
		if($(this).attr('target')){
			$(this).parent().addClass('blank');
		}
	});


	//통합검색 해상도 온오프
	function totalSearch(){
		if(winW >1024 && $('body').hasClass('main')){
			$('.total_search').hide();
		}
	}totalSearch();


	//모바일 소셜공유
	$('.share button').click(function(){
		$(this).next('span').toggle(0);
	});
	$('.share .aria_chk').click(function(){
		$('.share span').toggle(0);
	});


	//텍스트박스 열기 issue_box (재외국인등록 p)
	$('.texts_show_btn').click(function(){
		$(this).toggleClass('on');
		var aria = $(this).find('.aria_chk');
		var ariaText = aria.text();
		$(this).next('.issue_box').stop().slideToggle(function(){
			aria.text(ariaText == "내용닫혀있음" ? "내용펼쳐짐" : "내용닫혀있음");
		});
	});
	win.resize(function(){
		if(winW >= 768){
			$('.issue_box').show();
		}
	});


	// 푸터 기관노출
	$('.slot button').on('click', function(i){
		var aria = $(this).find('.aria_chk');
		var ariaText = aria.text();
		$(this).parents('.slot').find('div').toggle(0, function(){
			aria.text(ariaText == "링크메뉴닫힘" ? "링크메뉴열림" : "링크메뉴닫힘");
		});
	});


	// 외교정책 영토해양관련 독도 비디오
	$('.dokdo_video > div:first').show();
	$('.dokdo_video_select > a').click(function(e){
		e.preventDefault();

		var select = $('.dokdo_language option:selected'),
			selectAddress = select.val(),
			selectText = select.text();

		$('.in_frame iframe').attr({
			'src':selectAddress,
			'title':"대한민국의 아름다운 영토,독도 " + selectText + " 버전 "
		});
	});

	//파일업로드
    $(document).on('change', '.filebox .upload-hidden', function(){
		$(this).siblings('.upload-name').val('선택된 파일 없음');
        if(window.FileReader){
			if (!$(this)[0].files[0].type.match(/image\//)) return;
			var filename = $(this)[0].files[0].name;
			$(this).siblings('.upload-name').val(filename);
        } else {
            var filename = $(this).val().split('/').pop().split('\\').pop();
			$(this).siblings('.upload-name').val('선택된 파일 없음');
        }
    });

	//주한공관주소록 반응형 DT 높이 제어
	function worldDocDt(){
		winW = $(window).width();
		if(winW <= 768){
			$('.world_info_doc tbody th').each(function(){
				$(this).height($(this).next('td').height());
			});
		}
	}worldDocDt();

	//공통 게시판 검색 반응형 select 갯수 분기
	var top_search_select = $('.top_search');
	if(top_search_select.length > 0){
		top_search_select.each(function(){
			if($(this).find('select').length == 3){
				$(this).addClass('max');
				$(this).find('select:eq(1)').css({'margin-right':'10px', 'border-right':'1px solid #8d8d8d'})
			}else if($(this).find('select').length == 2){
				$(this).find('select:eq(0)').css({'margin-right':'10px', 'border-right':'1px solid #8d8d8d'})
			}else if($(this).find('select').length == 1){
				$(this).addClass('min');
			}
		});
	}

	//팝업 열기
	$('.layer_obj a').on('click', function(e){
		e.preventDefault();
		var thatLink = $(this).attr('href');
		$(this).attr('title','팝업 레이어 열림');
		$(thatLink).show(0, function(){
			$(this).attr('tabindex','0' );
			$(this).attr('title', '레이어 열림'); // 팝업 레이어 상태 추가
			$(this).focus();
		});
		layer_btn = $(this);
	});
	//팝업 닫기
	$('.pop_close').on('click', function(){
		$('.dim_pop').hide(0, function(){
			$(this).removeAttr('tabindex');
			$(this).attr('title', '레이어 닫힘'); // 팝업 레이어 상태 추가
			$('.layer_obj a').attr('title','팝업 레이어 닫힘');
		});
		layer_btn.focus();
		
	});

	//레이어팝업 리사이징
	function popResize(){
		var winW = win.width();
		var winH = win.height();
		if(winW <= 768){
			$('.dim_pop .body').css({
				'max-height':winH - $('.dim_pop .top').height() - 100
			});

			if(winW <= 480){
				$('.dim_pop .body').css({
					'max-height':winH - $('.dim_pop .top').height() - 14
				});
			}
		}
	}popResize();
	
	//외교부소개 - 역대외교부장관
	$('.captain_history button').on('click', function(){
		var self = $(this);
		var selfText = self.find('span');
		self.toggleClass('on');
		selfText.text(selfText.text() == "프로필 더보기" ? "프로필 닫기" : "프로필 더보기");
		self.parents('.info').next('.detail').stop().slideToggle(function(){
			if($(this).is(':visible')){
				$(this).prepend( "<p class='aria_chk'>상세프로필 열림</p>" );
			}else{
				$(this).find('.aria_chk').remove();
			}
		});
	});

	//홈페이지 도우미 - 사이트맵
	$('.site_map .g_sub_list2').masonry({
		itemSelector: '.unit',
		percentPosition: true,
		gutter: 80
	});

	//국민참여 - 장관과의대화(자기민원보기)
	$('.my_ddd').on('click', function(){
		$('.iframe_wrap iframe').attr('src', 'http://www.epeople.go.kr/jsp/user/frame/pc/mycvreq/UPcMyCvreqList.jsp?anc_code=1262000&channel=1262000&menu_code=PC005');
	});

	//버튼 하위 갯수 체크하여 작은해상도 대응
	/*
		$('.btn_wrap').each(function(){
			if($(this).find('a').length == 2){
				$(this).addClass('ea')
			}
		});
	*/

	

	//외교부소개 - 조직도
	function orgChart(){

		$('.org_chart > ol > li.dep1').each(function(){

			var self = $(this);
			var self_strong = self.find('strong').first();

			self.find('> .dep2').each(function(i){
			/*
				$(this).find('> li').each(function(ii){
					var that = $(this);
					var dep3 = that.find('.dep3')
					//2뎁스 세로크기 지정
					if(that.find('ul').length > 0){
						that.find('> strong').height(
							(dep3.height() * dep3.size()) + (dep3.size() * 20) - 20
						);
					}else{
						that.find('> strong').height(30);
					}
				});
			*/
				self_strong.height($(this).height() * (i+1) + 4); //1뎁스 세로크기 지정

			});

			//텍스트 가운데 정렬
			
			self_strong.find('span').css({
				'top':'50%',
				'margin-top': - ($(this).find('span').height()/2) + 'px'
			});
			
		});

	}orgChart();

	$('.org_chart .dep3, .org_chart .dep4').each(function(){
		if($(this).find('li').length <= 0){
			$(this).remove();
		}
	});
	$('.org_chart .dep3').each(function(){
		if(!$(this).find('ul').length >= 1){
			$(this).find('> li').addClass('dep4_no')
		}
	});
	var dep100 = $('.dep100');
	$('.t_indent50').append("<ul class='dep3 depth_line'></ul>");
	$('.depth_line').append(dep100);














	//외교정책 - 자주묻는질문(아코디언)
	$('.aco_list dt').on('click', function(){
		$(this).next('dd').stop().slideToggle(function(){
			if($(this).is(':visible')){
				$(this).prev('dt').find('.aria_chk').text('하위내용열림');
			}else{
				$(this).prev('dt').find('.aria_chk').text('하위내용닫힘');
			}
		});
	});


	//국가검색
	//PC
	var $imgc = $('.map_content');
	$('#nation_map area').on('mouseenter focusin', function(){
		var that = $(this);
		thatIdx = that.index();
		$imgc.hide().parent('a').attr('title','');
		$imgc.eq(thatIdx).show().parent('a').attr('title','메뉴 선택됨');
	}).on('focusout', function(){
		$imgc .hide().parent('a').attr('title','');
	}).click(function(){
		$imgc.removeClass('select').parent('a').attr('title','');
		$imgc.eq(thatIdx).addClass('select').parent('a').attr('title','메뉴 선택됨');
	});

	//모바일
	var select_nation = $('.select_nation');
	select_nation.find(' > a').on('click', function(){
		var $self = $(this);
		$self.parent().toggleClass('on');
		$self.next('ul').stop().slideToggle();
		
		$self.next('ul').find('a').on('click', function(e){
			var text = $(this).text();
			var index = $(this).parent('li').index();
			$self.parent().removeClass('on');
			$self.text(text).attr('title','선택됨').next('ul').stop().slideUp();
			$('.m_nation_map > div').hide().eq(index).show();
			$('#packaging03 .m_map_box .m_nation_map > div, #packaging03 .m_map_box .m_nation_map, #packaging03 .m_map_box').css({'background':'none'});
		});
	});

	//국가 개요
   $(".nation_summary:first-child h4").addClass("open");
	$('.nation_summary:first > div').css('display','block');
	
	$('.relationship h4').click(function() {
		$('.relationship h4').find('a').attr('title', '컨텐츠 닫힘'); 
		$('.summary_cont').slideUp();
		if ($(this).hasClass('open')) {
			$(this).next('.summary_cont').slideUp();
			$('.relationship h4').removeClass('open');
			
		} else {
			$(this).next('.summary_cont').slideToggle();
			$('.relationship h4').removeClass('open');
			$(this).toggleClass('open');
			//$(this).attr('title', '컨텐츠 열림'); 
			$(this).find('a').attr('title', '컨텐츠 열림'); 
		}
	});






});// $ end