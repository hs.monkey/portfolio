$(function () {

	var win = $(window);
	var winW = win.width();
	var winH = win.height();
	var reWidth = $(window).width();
	var layer_btn; // 레이어팝업 키

	//디바이스 체크
	var filter = 'win16|win32|win64|mac|macintel|linux i686';
	if(navigator.platform){
		if(filter.indexOf(navigator.platform.toLowerCase()) < 0){
			$('html').addClass('mob');
		}else{
			$('html').addClass('pc');
		}
	}
	if($('html').hasClass('mob')){
		if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){
			$('html').addClass('ios');
		}else{
			$('html').addClass('android');
		}
	}

	//탭 메뉴 접근성 강화
	if ($('.tabs').length) {
		var tabOnText = '메뉴선택됨';
		if($('.tabs li').hasClass('active')){
			$('.active > a').attr({'title':tabOnText});
		}
		$('.tabs a').on('click',function(e) {
			var thisHref = $(this).attr('href');
			$(this).parent('li').addClass('active').siblings().removeClass('active');
			$(this).parent('li').siblings().find(' > a').removeAttr('title');
			$(this).attr({'title':tabOnText});
			$(thisHref).addClass('active').siblings().removeClass('active');
		});
	}

	//모바일 소셜공유
	$('.share button').click(function() {
		$(this).next('span').toggle(0);
	});
	$('.share .aria_chk').click(function() {
		$('.share span').toggle(0);
	});
	

	//메인페이지 효과
	//--bg
	var btn_lnb= $('#main .lnb .tabs li');
	$(btn_lnb) .click(function(){
		var this_index= $(this).index(),
			arr_body_bgClass= ['bodybg_1','bodybg_2','bodybg_3','bodybg_4'],
			arr_body_bgClass2= ['bodybg_5887','bodybg_5889','bodybg_5890','bodybg_5892'],
			body_bg= arr_body_bgClass[this_index],
			body_bg2= arr_body_bgClass2[this_index];
		$('body').removeClass().addClass(body_bg+' '+body_bg2);
	});

	//--수직 스크롤
	if ($('#main').length) {
		var $content= $('#content');
		$content.on("mousewheel DOMMouseScroll", function (e) {
			e.preventDefault();
			var event = e.originalEvent;
			var delta = 0;
			if (!event) {event = window.event};
			if (event.wheelDelta) {
				delta = event.wheelDelta / 120;
				if (window.opera) delta = -delta;
			} else if (event.detail) {
				delta = -event.detail / 3;
			}
			var lnb_btn_toUp= $('.lnb li.active').next('li').find('a'),
				lnb_btn_toDown= $('.lnb li.active').prev('li').find('a');
			// 마우스휠을 위에서 아래로
			if (delta < 0) {
				lnb_btn_toUp .click();
			// 마우스휠을 아래에서 위로
			} else {
				lnb_btn_toDown .click();
			}
		});
	}

	//--각 페이지별
	function eff_p1(){
		$('#main .section .obj_main_airplane').stop().animate({
			opacity:1,
			left:'-=20',
			top:'-=20'
		},800);
		$('#main .section .obj_main_airballoon').stop().animate({
			opacity:1,
			left:'+=20',
			top:'-=20'
		},800);
		$('#main .section .obj_main_desk').stop().animate({
			opacity:1,
			top:'-=20'
		},800,'linear');
		$('#main .section .obj_main_ship').stop().animate({
			opacity:1,
			left:'-=20'
		},800,'linear');

		var balloon= "#main .section .obj_balloon_";
		$(balloon+'1').stop().delay(800).animate({
			opacity:1,
			top:'+=5'
		},200,function(){
			$(balloon+'2').stop().animate({
				opacity:1,
				top:'+=5'
			},200,function(){
				$(balloon+'3').stop().animate({
					opacity:1,
					top:'+=5'
				},200,function(){
					$(balloon+'4').stop().animate({
						opacity:1,
						top:'+=5'
					},200,function(){
						$(balloon+'5').stop().animate({
							opacity:1,
							top:'+=5'
						},200);
					});
				});
			});
		});
	}

	var obj= $('[class^="obj_"]');
	if($(btn_lnb[0]) .hasClass('active')){
		eff_p1();
	}
	for(var i=0; i<4; i++){
		$(btn_lnb[i]) .on('click', function(){
			obj.stop().removeAttr('style');
			eff_p1();
		});
	}

	//모바일 landscape 대응 --말풍선 효과
	$( window ).resize(function(){
		var re_winW= win.width();
		if (winW != re_winW && $('html').hasClass('mob')){
			$('.lnb li.active') .click();
		};
		winW= re_winW;
	});
	
	//내비게이션
	if($('.gnb').length) {
		$('.btn_gnb').on('click',function() {
			$('.gnb').addClass('active_gnb');
			$('body').css('position','fixed');
		});
		$('.btn-menu-close').on('click',function() {
			$('.gnb').removeClass('active_gnb');
			$('body').removeAttr('style');
		});
	}
	$( window ).resize(function(){
		var re_winW= win.width();
		if (re_winW>1200 ){
			$('.gnb').removeClass('active_gnb');
			$('body').removeAttr('style');
		}
	});
	
	//조직도--1뎁스 높이 설정
	if($('.chart_ognz').length){ 
		$('.chart_ognz') .each(function(){
			var contH= $(this).find('.cont_list').height();
			$(this).find('.tit_sec') .height(contH);
		});
	}
	$( window ).resize(function(){
		if($('.chart_ognz').length){ 
		$('.chart_ognz') .each(function(){
			var contH= $(this).find('.cont_list').height();
			$(this).find('.tit_sec') .height(contH);
		});
	}
	});
	
	
	// !![D]!!  개발시 삭제
//	if (winW>1200){
//			$('html').addClass('pc');
//		}else{
//			$('html').addClass('mob');
//		}
//	$( window ).resize(function(){
//		var re_winW= win.width();
//		$('html').removeClass();
//		if (re_winW>1200){
//			$('html').addClass('pc');
//		}else{
//			$('html').addClass('mob');
//		}
//	});




});// $ end
